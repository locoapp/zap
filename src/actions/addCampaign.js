import store from '../store'
import { notification } from 'antd';
import {addNewCampaign,alterConfirmCampaignModal} from './index'
export const addCampaign = () => {
    return (dispatch) => {
        let state = store.getState();
        let isLanguageFieldActive = state.Settings.isLanguageFieldActive
        let dataSource = isLanguageFieldActive ? state.Table.dataSource : state.Table.allDataSource
        let newDataSource = Object.assign(dataSource,[])
        if(newDataSource.length==0)return
        newDataSource.forEach(data => {
            try{
                let campaign = formCampaign(data)
                dispatch(addNewCampaign(campaign))
            }
            catch(e){
                notification.warn({
                    message: e,
                    className:'notification_error'
                  });
            }
           
        }); 
        dispatch(alterConfirmCampaignModal(true) )
    }
}

 function formCampaign (data){
     
    let state = store.getState();
    let isLanguageFieldActive = state.Settings.isLanguageFieldActive
    const {create_format} = state.Targets
    //iterates over dtasource to create camapign objects
    let {uid,language,title,message,type,time} = data
    if(title.includes("Enter the title") ||title == "" || 
         message.includes("Enter the message") ||message == "" ){
             throw "Invalid Input Fields"
    }
    let {
        date,
        defaultNameDateFormat,
        defaultDateFormat,
        defaultTimeFormat,
        sender_name
    } = state.Defaults

    let nameDate = date.format(defaultNameDateFormat)
    let whenDate = date.format(defaultDateFormat)
    let Time = time.format(defaultTimeFormat)
    let name = `${language}-${nameDate}-${type}-${Time}`
    let when = `${whenDate} ${Time}`
    let current_campaign = JSON.parse(JSON.stringify(create_format))//deep cloning
    current_campaign['uid'] = uid
    current_campaign['name'] = name
    current_campaign['when'] = when
    current_campaign['content']['title'] = title
    current_campaign['content']['subject'] = message
    current_campaign['content']['body'] = message
    current_campaign['content']['sender_name'] = sender_name
    current_campaign['content']['platform_specific']['android'] = {'type':'clevertap','title':title,'message':message}
    current_campaign['where']['common_profile_properties']['profile_fields'] = language!=="All" ? [{"name": "Language","operator": "equals","value": language}] : []
    return current_campaign
}