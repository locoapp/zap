import * as api from "./api";
import { notification } from "antd";
import * as actionTypes from "../actionTypes";

export const setAdhocGrantLoading = bool => ({
	type: actionTypes.SET_GRANT_COIN_LOADING,
	payload: bool
});

export const grantCoins = values => {
	return async dispatch => {
		dispatch(setAdhocGrantLoading(true));
		try {
			let response = await api.grantCoins(values);
			notification.success({
				message: " Coins Added Successfully",
				description: `${response.data.amount} coins granted to ${
					values.username
				}`,
				className: "notification_error"
			});
		} catch (e) {
			notification.error({
				message: "Granting Coins Failed",
				description: e.message,
				className: "notification_error"
			});
		}
		dispatch(setAdhocGrantLoading(false));
	};
};
