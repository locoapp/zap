import * as actionTypes from "../actionTypes";
import store from "../store";
import * as api from "./api";
import { notification } from "antd";

export const changeWhen = when => ({
	type: actionTypes.CHANGE_WHEN,
	payload: when
});
export const changeDate = date => ({
	type: actionTypes.CHANGE_DATE,
	payload: date
});
export const changeTime = time => ({
	type: actionTypes.CHANGE_TIME,
	payload: time
});

export const changeType = type => ({
	type: actionTypes.CHANGE_TYPE,
	payload: type
});

export const toggleLanguageField = checked => ({
	type: actionTypes.TOGGLE_LANGUAGE_FIELD,
	payload: checked
});

export const tableAdd = () => ({
	type: actionTypes.TABLE_ADD
});

export const tableDelete = key => ({
	type: actionTypes.TABLE_DELETE,
	payload: key
});

export const tableSave = (row, type) => {
	return {
		type: actionTypes.TABLE_SAVE,
		payload: { row, type }
	};
};

export const cleanTable = () => ({
	type: actionTypes.CLEAN_TABLE
});
export const addNewCampaign = campaign => {
	return {
		type: actionTypes.ADD_CAMPAIGN,
		payload: campaign
	};
};
export const alterCampaignModal = payload => ({
	type: actionTypes.ALTER_CAMPAIGN_MODAL,
	payload: payload
});
export const alterConfirmCampaignModal = payload => ({
	type: actionTypes.ALTER_CONFIRM_CAMPAIGN_MODAL,
	payload: payload
});
export const alterContestCampaignModalActive = bool => ({
	type: actionTypes.ALTER_CONTEST_CAMPAIGN_MODAL,
	payload: bool
});
export const setCampaignCreated = index => ({
	type: actionTypes.SET_CAMPAIGN_CREATED,
	payload: index
});
export const setCampaignFailed = index => ({
	type: actionTypes.SET_CAMPAIGN_FAILED,
	payload: index
});
export const setLoading = payload => ({
	type: actionTypes.SET_LOADING,
	payload: payload
});
export const setCampaignsLoading = bool => ({
	type: actionTypes.SET_CAMPAIGNS_LOADING,
	payload: bool
});
export const setAllClicked = payload => ({
	type: actionTypes.SET_ALL_CLICKED
});
export const updateAllCampaigns = payload => ({
	type: actionTypes.UPDATE_ALL_CAMPAIGNS,
	payload: payload
});

export const createAllCampaigns = () => {
	return async dispatch => {
		let { campaigns } = store.getState();
		//https://codeburst.io/javascript-async-await-with-foreach-b6ba62bbf404
		const asyncForEach = async (array, callback) => {
			for (let index = 0; index < array.length; index++) {
				await callback(array[index], index, array);
			}
		};
		await asyncForEach(campaigns, async (c, index) => {
			await dispatch(createCampaign(index));
		});
	};
};

export const fetchCampaigns = uid => {
	return async dispatch => {
		dispatch(setCampaignsLoading(true));
		let response = await api.getAllCampaigns(uid);
		let campaigns = response
			.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
			.splice(0, 100);
		dispatch(updateAllCampaigns(campaigns));
		dispatch(setCampaignsLoading(false));
	};
};

export const createCampaign = index => {
	return async dispatch => {
		dispatch(setAllClicked());
		dispatch(setLoading({ index: index, bool: true }));
		let { campaigns } = store.getState();
		let campaign = campaigns[index];
		if (
			campaign.content.title.includes("Enter the title") ||
			campaign.content.title == "" ||
			campaign.content.subject.includes("Enter the message") ||
			campaign.content.subject == ""
		) {
			setTimeout(() => dispatch(setCampaignFailed(index)), 500);
			notification.warning({
				message: "Campaign has Invalid Fields",
				description: `title : ${campaign.content.title} \n \n message : ${
					campaign.content.subject
				}`,
				className: "notification_error"
			});
			return;
		}

		try {
			let response = await api.createCampaign(campaign);
			if (response && response.status == "success") {
				dispatch(setCampaignCreated(index));
				notification.success({
					message: "Campaign created ",
					description: `Campaign Id: ${response.id} `,
					className: "notification_success"
				});
			}
		} catch (e) {
			setTimeout(() => dispatch(setCampaignFailed(index)), 500);
			notification.error({
				message: "Campaign creation Failed",
				description: e.message,
				className: "notification_error"
			});
		}
	};
};
