import * as api from "./api";
import * as actionTypes from "../actionTypes";
import { notification } from "antd";
import { fetchCampaigns, alterContestCampaignModalActive } from './index'
import moment from "moment";
import store from "../store";
export const alterContestModal = bool => ({
  type: actionTypes.ALTER_CONTEST_MODAL,
  payload: bool
});

export const updateDate = date => ({
  type: actionTypes.UPDATE_DATE,
  payload: date
});
export const updateShowtime = showtime => ({
  type: actionTypes.UPDATE_SHOW_TIME,
  payload: showtime
});

export const updateTokens = payload => ({
  type: actionTypes.UPDATE_TOKENS,
  payload: payload
});

export const populateContests = contests => ({
  type: actionTypes.POPULATE_CONTESTS,
  payload: contests
});

export const setNormalLoco = bool => ({
  type: actionTypes.SET_NORMAL_LOCO,
  payload: bool
});

export const populateTypes = types => ({
  type: actionTypes.POPULATE_TYPES,
  payload: types
});

export const setContestLoading = bool => ({
  type: actionTypes.SET_CREATE_CONTEST_LOADING,
  payload: bool
});

export const populateContestForeigns = categories => ({
  type: actionTypes.POPULATE_CONTEST_FOREIGNS,
  payload: categories
});

export const populateContesDefaults = categories => ({
  type: actionTypes.POPULATE_CONTEST_DEFAULTS,
  payload: categories
});
export const setContestForCampaign = contest => ({
    type:actionTypes.SET_CONTEST_FOR_CAMPAIGN,
    payload:contest
})

export const setLoading = bool => ({
  type: actionTypes.SET_CONTEST_LOADING,
  payload: bool
});
export const setCategoryLoading = bool => ({
  type: actionTypes.SET_CONTEST_CATEGORY_LOADING,
  payload: bool
});

export const resetCategorySelected = () => ({
  type: actionTypes.RESET_CATEGORY_SELECTED
});

export const setContestCategory = category => {
  return async dispatch => {
    let { defaults } = store.getState().Contests;
    dispatch(
      updateShowtime(moment(defaults[category.name]["time"], "HH:mm:ss"))
    );
    dispatch({
      type: actionTypes.SET_CONTEST_CATEGORY,
      payload: category
    });
  };
};

export const fetchContestForeigns = () => {
  return async dispatch => {
    try {
      let foreigns = await api.getContestForeigns();
      dispatch(populateContestForeigns(foreigns));
    } catch (e) {
      dispatch(setCategoryLoading(true));
      notification.open({
        message: "Fetching Foreigns Failed",
        description: e.message,
        className: "notification_error"
      });
    }
  };
};

export const fetchContestDefaults = () => {
  return async dispatch => {
    try {
      let defaults = await api.getContestDefaults();
      let flattenedDefaults = defaults.reduce((acc, item) => {
        acc[item["contest_category_name"]] = item;
        return acc;
      }, {});
      dispatch(populateContesDefaults(flattenedDefaults));
      dispatch(setCategoryLoading(false));
    } catch (e) {
      notification.error({
        message: "Fetching Defaults Failed",
        description: e.message,
        className: "notification_error"
      });
    }
  };
};

export const fetchContests = () => {
  return async dispatch => {
    try {
      let contests = await api.getContests();
      dispatch(populateContests(contests.data));
      dispatch(populateTypes(contests.types));
    } catch (e) {
      notification.error({
        message: "Fetching Contests Failed",
        description: e.message,
        className: "notification_error"
      });
    }
  };
};
export const contestCreateSuccess = () => ({
  type: actionTypes.CONTEST_CREATE_SUCCESS
});

export const createNewContest = contest => {
  return async dispatch => {
    try {
      contest.created_at = new Date();
      contest.updated_at = new Date();
      let response = await api.createContest(contest);
      notification.success({
        message: "Contest Created Successfully",
        description: `UID: ${response.uid}`,
        className: "notification_error"
      });
      dispatch(contestCreateSuccess());
    } catch (e) {
      notification.error({
        message: "Creating Contests Failed",
        description: e.message,
        className: "notification_error"
      });
    }
  };
};

export const checkContestCampaigns = (uid,contest) => {
    return async dispatch => {
        dispatch(setContestForCampaign(contest))
        await dispatch(fetchCampaigns(uid))
        dispatch(alterContestCampaignModalActive(true))

    }
}