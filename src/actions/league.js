import * as actionTypes from "../actionTypes";
import * as api from "./api";
import { message } from "antd";
import { Storage } from "aws-amplify";
import store from "../store";
export const setLoading = bool => ({
	type: actionTypes.SET_LEAGUE_LOADING,
	payload: bool
});

export const updateAllLeagues = leagues => ({
	type: actionTypes.UPDATE_ALL_LEAGUES,
	payload: leagues
});

export const updateLeagueQuestions = questions => ({
	type: actionTypes.UPDATE_LEAGUE_QUESTIONS,
	payload: questions
});

export const updateCurrentLeague = league => ({
	type: actionTypes.UPDATE_CURRENT_LEAGUE,
	payload: league
});
export const fetchLeagues = () => {
	return async dispatch => {
		let {
			League: { offset, limit }
		} = store.getState();
		let leagues = await api.getAllLeagues(offset, limit);
		dispatch(updateAllLeagues(leagues));
	};
};

export const fetchLeagueQuestions = game_uid => {
	return async dispatch => {
		let questions = await api.getAllQuestions(game_uid);
		dispatch(updateLeagueQuestions(questions));
	};
};

export const setQuestionLoading = bool => ({
	type: actionTypes.SET_LEAGUE_QUESTIONS_LOADING,
	payload: bool
});

export const getNextLeagues = () => {
	return async dispatch => {
		let {
			League: { offset, limit, previousOffset }
		} = store.getState();
		if (
			Object.keys(previousOffset).length > 0 &&
			Object.keys(offset).length == 0
		)
			return [];
		let leagues = await api.getAllLeagues(offset, limit);
		dispatch({ type: "append_next_leagues", payload: leagues });
	};
};

export const setCurrentLeague = data => ({
	type: actionTypes.SET_CURRENT_LEAGUE,
	payload: data
});

export const alterLeagueModal = bool => ({
	type: actionTypes.ALTER_LEAGUE_MODAL,
	payload: bool
});

export const createLeagueFailed = () => ({
	type: actionTypes.CREATE_LEAGUE_FAILED
});
export const uploadAnswersFailed = () => ({
	type: "upload_answers_failed"
});
export const alterCreateLeagueModal = bool => ({
	type: actionTypes.ALTER_CREATE_LEAGUE_MODAL,
	payload: bool
});

export const updateLeagueGame = values => {
	return async dispatch => {
		try {
			let response = await api.createNewLeagueGame(values);
			dispatch({ type: "append_created_league", payload: response });
			dispatch(setCreateLeagueLoading(false));
			dispatch(updateCurrentLeague(response));
		} catch (e) {
			message.error(e.message);
			dispatch(createLeagueFailed(e.message));
		}
	};
};

export const startPrizeDistribution = () => {
	return async dispatch => {
		try {
			dispatch({ type: "alter_locking_loading", payload: true });
			let { open_league } = store.getState().League;
			let response = await api.startPrizeDistribution(open_league.key);
			message.success(response.message);
			dispatch({ type: "update_open_league_status", payload: 30 });

			dispatch({ type: "alter_locking_loading", payload: false });
			dispatch({ type: actionTypes.ALTER_LEAGUE_MODAL, payload: false });
			dispatch({ type: "clear_previous_answer" });
		} catch (e) {
			message.error(e.message);
			dispatch({ type: "alter_locking_loading", payload: false });
		}
	};
};

export const setCreateLeagueLoading = bool => ({
	type: actionTypes.SET_CREATE_LEAGUE_LOADING,
	payload: bool
});
export const alterQuestionUploading = bool => ({
	type: "alter_question_loading",
	payload: bool
});

export const uploadAnswers = () => {
	return async dispatch => {
		try {
			dispatch({ type: "alter_answers_uploading", payload: true });

			let { answers, open_league } = store.getState().League;
			let response = await api.uploadAnswers(answers, open_league.key);
			message.success(response.message);
			dispatch({ type: "alter_answers_uploading", payload: false });
			dispatch({ type: actionTypes.ALTER_LEAGUE_MODAL, payload: false });
			dispatch({ type: "clear_previous_answer" });
		} catch (e) {
			message.error(e.message);
			dispatch(uploadAnswersFailed(e.message));
		}
	};
};

export const uploadQuestions = (game_uid, questions) => {
	return async dispatch => {
		try {
			dispatch(alterQuestionUploading(true));
			let response = await api.addGameQuestions(game_uid, questions);
			message.success("question_uploaded_successfully");
			dispatch(alterQuestionUploading(false));
			dispatch({ type: "question_uploaded_successfully" });
		} catch (e) {
			message.error(e.message);
			dispatch(createLeagueFailed(e.message));
		}
	};
};

export const updateImageData = stored => ({
	type: "update_league_image_data",
	payload: stored
});

export const s3Upload = file => {
	return async dispatch => {
		const filename = `${Date.now()}-${file.name}`;

		const stored = await Storage.put(filename, file, {
			contentType: file.type
		});
		let res = await Storage.get(stored.key, { level: "public" });
		let file_url = res.split("?")[0];
		let cdn_url = "https://static.getloconow.com" + file_url.split(".com")[1];
		dispatch(updateImageData(cdn_url));

		return res.split("?")[0];
	};
};
