import { API } from "aws-amplify";

function getTableName(table_type) {
	const devTable = process.env.NODE_ENV === "development" ? "dev" : "prod";
	switch (table_type) {
		case "contests":
			return devTable + "_contest_defaults";
		case "campaigns":
			return devTable + "_clevertap_campaigns";
	}
}

export const createCampaign = async campaign => {
	let response = await API.post("clevertap", "create.json", {
		body: campaign
	});
	return response;
};

export const stopCampaign = async id => {
	let response = await API.post("clevertap", "stop.json", {
		body: id
	});
	return response;
};

export const getContests = async () => {
	let response = await API.get("contest", "all_contests.json");
	return response;
};

export const getContestForeigns = async () => {
	let response = await API.get("contest", "all_contest_foreign.json");
	return response;
};

export const getContestDefaults = async () => {
	let response = await API.post("contest", "defaults.json", {
		body: {
			table_name: getTableName("contests")
		}
	});
	return response;
};

export const getAllCampaigns = async uid => {
	let body = {
		table_name: getTableName("campaigns")
	};
	if (uid !== undefined && uid !== null) {
		body["uid"] = uid;
	}
	let response = await API.post("contest", "defaults.json", {
		body: body
	});
	return response;
};

export const createContest = async contest => {
	let response = await API.post("contest", "create.json", {
		body: contest
	});
	return response;
};

export const grantCoins = async values => {
	let response = await API.post("contest", "grant_adhoc_coins.json", {
		body: values
	});
	return response;
};

//leagues

export const getAllLeagues = async (offset, limit) => {
	let response = await API.post("leagues", `getAllLeagues/?limit=${limit}`, {
		body: offset
	});
	return response;
};

export const getAllQuestions = async game_uid => {
	let response = await API.get("leagues", `${game_uid}/getAllQuestions/`);
	return response;
};

export const createNewLeagueGame = async values => {
	let response = await API.post("leagues", "add_new_game/", {
		body: values
	});

	return response;
};

export const startPrizeDistribution = async game_uid => {
	let response = await API.post("leagues", "lock_answers/", {
		body: {
			game_uid: game_uid
		}
	});
	return response;
};
export const addGameQuestions = async (game_uid, questions) => {
	let response = await API.post("leagues", `${game_uid}/add_game_questions/`, {
		body: questions
	});
	return response;
};

export const uploadAnswers = async (answers, game_uid) => {
	let response = await API.post("leagues", `${game_uid}/upload/answers/`, {
		body: answers
	});
	return response;
};
