import * as actionTypes from '../actionTypes'


//TODO:initial state from api
const initialState = {
   isGrantCoinLoading:false
}

const Adhoc = (state=initialState,action) => {
    switch(action.type){
        case actionTypes.SET_GRANT_COIN_LOADING:
            return {
                ...state,
                isGrantCoinLoading:action.payload
            }
       
        default:
            return state
    }
}

export default Adhoc