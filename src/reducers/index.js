import { combineReducers } from "redux";

import Defaults from "./Defaults";
import Targets from "./Targets";
import Settings from "./Settings";
import Table from "./Table";
import campaigns from "./campaigns";
import Contests from "./contests";
import Adhoc from "./adhoc";
import League from "./league";
const reducers = combineReducers({
	Defaults,
	Targets,
	Settings,
	Table,
	campaigns,
	Contests,
	Adhoc,
	League
});

export default reducers;
