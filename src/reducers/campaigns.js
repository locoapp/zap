import * as actionTypes from "../actionTypes";
//TODO:initial state from api
const initialState = [
	// {
	//     "name": "All-08-NOV-Night-21:55",
	//     "when": "20181108 21:55",
	//     "content": {
	//         "title":"10PM is Loco time!",
	//         "subject":"Join us in 5 minutes.",
	//         "body": "Join us in 5 minutes.",
	//         "sender_name": "Loco",
	//         "platform_specific": {
	//             "android": {
	//                 "type": "clevertap",
	//                 "title":"10PM is Loco time!",
	//                 "message":"Join us in 5 minutes."
	//             }
	//         }
	//     },
	//     "target_mode": "push",
	//     "where": {
	//         "common_profile_properties": {
	//             "profile_fields": [
	//             	]
	//         }
	//     },
	//     "devices": [
	//         "ios",
	//         "android"
	//     ]
	// },
	// {
	//     "name": "All-08-NOV-Night-21:55",
	//     "when": "20181108 21:55",
	//     "content": {
	//         "title":"10PM is Loco time!",
	//         "subject":"Join us in 5 minutes.",
	//         "body": "Join us in 5 minutes.",
	//         "sender_name": "Loco",
	//         "platform_specific": {
	//             "android": {
	//                 "type": "clevertap",
	//                 "title":"10PM is Loco time!",
	//                 "message":"Join us in 5 minutes."
	//             }
	//         }
	//     },
	//     "target_mode": "push",
	//     "where": {
	//         "common_profile_properties": {
	//             "profile_fields": [
	//             	]
	//         }
	//     },
	//     "devices": [
	//         "ios",
	//         "android"
	//     ]
	// }
];

const campaigns = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ADD_CAMPAIGN:
			action.payload["is_campaign_created"] = false;
			action.payload["is_loading"] = false;
			if (
				state.length > 0 &&
				state.filter(item => item.uid === action.payload.uid).length > 0
			) {
				return [...state];
			} else return [...state, action.payload];
		case actionTypes.SET_LOADING:
			let copyState = Object.assign(state, []);
			copyState[action.payload.index]["is_loading"] = action.payload.bool;
			return [...copyState];
		case actionTypes.SET_CAMPAIGN_CREATED:
			let newState = Object.assign(state, []);
			newState[action.payload]["is_campaign_created"] = true;
			return [...newState];
		case actionTypes.SET_CAMPAIGN_FAILED:
			let failedState = Object.assign(state, []);
			failedState[action.payload]["is_campaign_created"] = false;
			failedState[action.payload]["is_loading"] = false;
			return [...failedState];

		case actionTypes.TABLE_DELETE:
			let updatedState = state.filter(item => item.uid !== action.payload.uid);
			return [...updatedState];
		case actionTypes.CLEAN_TABLE:
			return [];
		case actionTypes.TABLE_SAVE:
			const { row } = action.payload;
			let saveState = state.filter(item => item.uid !== row.uid);
			return [...saveState];

		default:
			return state;
	}
};

export default campaigns;
