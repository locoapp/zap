import * as actionTypes from "../actionTypes";
import moment from "moment";

//TODO:initial state from api

const initialState = {
	stop_format: {
		id: 0
	},
	create_format: {
		name: "",
		when: "",
		content: {
			title: "",
			subject: "",
			body: "",
			sender_name: "",
			platform_specific: {
				android: {
					type: "clevertap",
					title: "",
					message: ""
				}
			}
		},
		target_mode: "push",
		where: {
			common_profile_properties: {
				profile_fields: []
			}
		},
		devices: ["ios", "android"]
	},
	campaigns: [],
	dataSource: []
};

const Targets = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.UPDATE_ALL_CAMPAIGNS:
			const campaigns = action.payload;
			const dataSource = campaigns.map(campaign => ({
				key: campaign.id,
				id: campaign.id,
				name: campaign.name,
				title: campaign.title,
				message: campaign.message,
				language: campaign.language,
				created_at: moment(campaign.time, "YYYYMMDD hh:mm")
					.format("MMM D, hh:mm A")
					.valueOf(),
				createdAt: moment(campaign.time, "YYYYMMDD hh:mm").format(
					"MMM D, hh:mm A"
				)
			}));
			return { ...state, campaigns: campaigns, dataSource: dataSource };
		default:
			return state;
	}
};

export default Targets;
