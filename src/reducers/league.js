import * as actionTypes from "../actionTypes";
import moment from "moment";

const initialState = {
	isLoading: true,
	isQuestionsLoading: true,
	areAnswersThere: false,
	leagues: [],
	isLeagueModalActive: false,
	currentQuestions: [],
	isQuestionUploading: false,
	open_league: {},
	isCreateLeagueLoading: false,
	isLockingLoading: false,
	currentLeague: {},
	isLeagueCreated: false,
	isCreateLeagueFailed: false,
	isCreateLeagueModalActive: false,
	isFileLocal: false,
	defaultLanguage: "English",
	stored_image: "",
	offset: {},
	answers: {},
	previousOffset: {},
	limit: 10,
	dataSource: [],
	status: {
		GO: 10,
		LOCKED: 20,
		ANSWERS_UPLOADED: 30,
		SCORE_CALCULATED: 40,
		SUCCESS: 50,
		CANCELLED: 60
	},
	languages: {
		Hindi: "0",
		English: "1",
		Marathi: "2",
		Bengali: "3",
		Telugu: "4",
		Tamil: "5"
	}
};
const League = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.SET_CREATE_LEAGUE_LOADING:
			return {
				...state,
				isCreateLeagueLoading: action.payload
			};

		case actionTypes.SET_LEAGUE_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		case "append_created_league":
			return {
				...state,
				leagues: [action.payload, ...state.leagues]
			};
		case actionTypes.SET_LEAGUE_QUESTIONS_LOADING:
			return {
				...state,
				isQuestionsLoading: action.payload
			};
		case actionTypes.UPDATE_ALL_LEAGUES:
			let previousOffset = state.offset;
			let leagues = action.payload.data;
			let dataSource = leagues.map(league => ({
				key: league.uid,
				name: league.name,
				starts_at: league.starts_at,
				ends_at: league.ends_at,
				buy_in: league.buy_in,
				prize_money: league.prize_money,
				result_announced_at: league.result_announced_at,
				status: league.status,
				startTime: moment(league.starts_at).format("MMM D, YYYY, hh:mm A  "),
				endTime: moment(league.ends_at).format("MMM D, YYYY, hh:mm A  "),
				resultTime: moment(league.result_announced_at).format(
					"MMM D, YYYY, hh:mm A  "
				)
			}));
			return {
				...state,
				offset: action.payload.offset,
				previousOffset: previousOffset,
				limit: action.payload.limit,
				leagues: leagues,
				dataSource: dataSource
			};
		case actionTypes.ALTER_LEAGUE_MODAL:
			return {
				...state,
				isLeagueModalActive: action.payload
			};
		case "update_open_league_status":
			let new_state = { ...state };
			new_state.open_league.status = action.payload;
			return new_state;
		case actionTypes.UPDATE_LEAGUE_QUESTIONS:
			let areAnswersThere = action.payload.every(question => {
				switch (question.type) {
					case 1:
						if (
							question.answer_option == undefined ||
							question.answer_option == null
						)
							return false;
						else return true;
					case 2:
						if (
							question.answer_input == undefined ||
							question.answer_input == null
						)
							return false;
						else return true;
					default:
						return false;
				}
			});
			return {
				...state,
				currentQuestions: action.payload,
				areAnswersThere: areAnswersThere
			};

		case actionTypes.SET_CURRENT_LEAGUE:
			return {
				...state,
				open_league: action.payload
			};
		case actionTypes.ALTER_CREATE_LEAGUE_MODAL:
			return {
				...state,
				isCreateLeagueModalActive: action.payload
			};
		case actionTypes.UPDATE_CURRENT_LEAGUE:
			let created_league = action.payload;
			let add_data = {
				key: created_league.uid,
				name: created_league.name,
				starts_at: created_league.starts_at,
				ends_at: created_league.ends_at,
				buy_in: created_league.buy_in,
				prize_money: created_league.prize_money,
				result_announced_at: created_league.result_announced_at,
				status: created_league.status,
				startTime: moment(created_league.starts_at).format(
					"MMM D, YYYY, hh:mm A  "
				),
				endTime: moment(created_league.ends_at).format(
					"MMM D, YYYY, hh:mm A  "
				),
				resultTime: moment(created_league.result_announced_at).format(
					"MMM D, YYYY, hh:mm A  "
				)
			};
			return {
				...state,
				currentLeague: action.payload,
				leagues: [created_league, ...state.leagues],
				dataSource: [add_data, ...state.dataSource],
				isLeagueCreated: true,
				isCreateLeagueFailed: false
			};
		case actionTypes.CREATE_LEAGUE_FAILED:
			return {
				...state,
				isCreateLeagueFailed: true,
				isLeagueCreated: false
			};
		case "alter_is_file_local":
			return {
				...state,
				isFileLocal: action.payload
			};
		case "alter_question_loading":
			return {
				...state,
				isQuestionUploading: action.payload
			};
		case "change_preview_language":
			return {
				...state,
				defaultLanguage: action.payload
			};
		case "alter_locking_loading":
			return {
				...state,
				isLockingLoading: action.payload
			};
		case "question_uploaded_successfully":
			return {
				...state,
				isLeagueModalActive: false,
				isFileLocal: false
			};
		case "update_league_image_data":
			return {
				...state,
				stored_image: action.payload
			};
		case "append_next_leagues":
			let prevOffset = state.offset;
			let new_leagues = action.payload.data.map(league => ({
				key: league.uid,
				name: league.name,
				starts_at: league.starts_at,
				ends_at: league.ends_at,
				buy_in: league.buy_in,
				prize_money: league.prize_money,
				result_announced_at: league.result_announced_at,
				status: league.status,
				startTime: moment(league.starts_at).format("MMM D, YYYY, hh:mm A  "),
				endTime: moment(league.ends_at).format("MMM D, YYYY, hh:mm A  "),
				resultTime: moment(league.result_announced_at).format(
					"MMM D, YYYY, hh:mm A  "
				)
			}));
			return {
				...state,
				offset: action.payload.offset,
				previousOffset: prevOffset,
				limit: action.payload.limit,
				leagues: [...state.leagues, ...action.payload.data],
				dataSource: [...state.dataSource, ...new_leagues]
			};
		case "update_answer_option":
			return {
				...state,
				answers: {
					...state.answers,
					[action.payload.question_uid]: {
						answer_option: action.payload.answer_option,
						type: 1
					}
				}
			};
		case "update_answer_input":
			return {
				...state,
				answers: {
					...state.answers,
					[action.payload.question_uid]: {
						answer_input: action.payload.answer_input,
						type: 2
					}
				}
			};
		case "alter_answers_uploading":
			return {
				...state,
				isAnswersUploading: action.payload
			};
		case "upload_answers_failed":
			return {
				...state,
				isAnswersUploading: false
			};
		case "clear_previous_answer":
			return {
				...state,
				answers: {}
			};
		default:
			return state;
	}
};

export default League;
