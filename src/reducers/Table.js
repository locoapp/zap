import * as actionTypes from "../actionTypes";
import moment from "moment";
import uuidv4 from "uuid/v4";

//TODO:initial state from api
const initialState = {
	campaignColumns: [
		{
			title: "Name",
			dataIndex: "name",
			key: "name"
		},
		{
			title: "ID",
			dataIndex: "id",
			key: "id"
		},
		{
			title: "Title",
			dataIndex: "title",
			key: "age"
		},
		{
			title: "Created At",
			dataIndex: "createdAt",
			key: "created_at",
			defaultSortOrder: "descend",
			sorter: (a, b) => a.created_at - b.created_at
		},
		{
			title: "Message",
			dataIndex: "message",
			key: "message"
		},

		{
			title: "Language",
			dataIndex: "language",
			key: "language"
		}
	],
	leagueColumns: [
		{
			title: "Buy In",
			dataIndex: "buy_in",
			key: "buy_in"
		},
		{
			title: "Prize",
			dataIndex: "prize_money",
			key: "prize_money"
		},
		{
			title: "Start Time",
			dataIndex: "startTime",
			key: "startTime",
			defaultSortOrder: "descend",
			sorter: (a, b) => a.starts_at - b.starts_at
		},
		{
			title: "Ends Time",
			dataIndex: "endTime",
			key: "endTime",
			defaultSortOrder: "descend",
			sorter: (a, b) => a.ends_at - b.ends_at
		},
		{
			title: "Results At",
			dataIndex: "resultTime",
			key: "resultTime",
			defaultSortOrder: "descend",
			sorter: (a, b) => a.result_announced_at - b.result_announced_at
		}
	],

	columns: [
		{
			title: "TITLE",
			dataIndex: "title",
			editable: true
		},
		{
			title: "MESSAGE",
			dataIndex: "message",
			editable: true
		}
	],
	dataSource: [
		{
			key: "0",
			uid: uuidv4(),
			language: "All",
			title: "Enter the title",
			message: "Enter the message",
			type: "Day",
			time: moment()
		}
	],
	allDataSource: [
		{
			key: "0",
			uid: uuidv4(),
			language: "All",
			title: "Enter the title ",
			message: "Enter the message",
			type: "Day",
			time: moment()
		}
	],
	count: 2,
	isCampaignsLoading: true
};

const Table = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.TABLE_ADD:
			const dataToAdd = {
				key: state.count,
				uid: uuidv4(),
				language: "All",
				title: "Enter the title",
				message: "Enter the message",
				type: "Day",
				time: moment()
			};
			return {
				...state,
				dataSource: [...state.dataSource, dataToAdd],
				count: state.count + 1
			};
		case actionTypes.TABLE_DELETE:
			return {
				...state,
				dataSource: state.dataSource.filter(
					item => item.key !== action.payload.key
				)
			};
		case actionTypes.CLEAN_TABLE:
			return {
				...state,
				dataSource: [
					{
						key: "0",
						uid: uuidv4(),
						language: "All",
						title: "Enter the title",
						message: "Enter the message",
						type: "Day",
						time: moment()
					}
				]
			};
		case actionTypes.TABLE_SAVE:
			const { type, row } = action.payload;
			const newData = [...state[type]];
			const index = newData.findIndex(item => row.uid === item.uid);
			const item = newData[index];
			newData.splice(index, 1, {
				...item,
				...row,
				uid: uuidv4()
			});
			return {
				...state,
				[type]: newData
			};
		case actionTypes.CHANGE_TYPE:
			return {
				...state,
				type: action.payload
			};
		case actionTypes.SET_CAMPAIGNS_LOADING:
			return {
				...state,
				isCampaignsLoading: action.payload
			};
		default:
			return state;
	}
};

export default Table;
