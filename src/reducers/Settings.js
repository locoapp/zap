import * as actionTypes from '../actionTypes'


//TODO:initial state from api
const initialState = {
   isLanguageFieldActive:true,
   createAllClicked:false,
   isConfirmCampaignsActive:false,
   isCreateCampaignActive:false
}

const Settings = (state=initialState,action) => {
    switch(action.type){
        case actionTypes.TOGGLE_LANGUAGE_FIELD:
            return {
                ...state,
                isLanguageFieldActive:action.payload
            }
        case actionTypes.ALTER_CONFIRM_CAMPAIGN_MODAL:
            return {
                ...state,
                isConfirmCampaignsActive:action.payload
            }
        case actionTypes.SET_ALL_CLICKED:
            return {
                ...state,
                createAllClicked:true
            }
        case actionTypes.ALTER_CAMPAIGN_MODAL:
            return {...state,isCreateCampaignActive:action.payload}
        default:
            return state
    }
}

export default Settings