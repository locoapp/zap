import * as actionTypes from '../actionTypes'
import moment from 'moment'


//TODO:initial state from api
const initialState = {
    'defaultLanguage':'All',
    'selectedLanguage':'All',
    'languages':['All','Hindi','Marathi','Bengali','Tamil','English','Telugu'],
    'quiz_types':['Story','Connect','Day','Night','Speed','Bazaar','Promo','Percent','Super','Googly','Pic'],
    'selected_quiz_type':'Day',
    'sender_name':'Loco',
    'devices':['ios','android'],
    'defaultNameDateFormat':'DD MMM',
    'defaultDateFormat':'YYYYMMDD',
    'date':moment(),
    'isAllActive':true,
    'defaultTimeFormat':'HH:mm'

}

const Defaults = (state=initialState,action) => {
    switch(action.type){
        case actionTypes.CHANGE_WHEN:
            return {
                ...state,
                ...action.payload
            }
        case actionTypes.CHANGE_DATE:
            return {
                ...state,
                "date":action.payload
            }
       
        default:
            return state
    }
}

export default Defaults