import * as actionTypes from "../actionTypes";
import moment from "moment";

//TODO:initial state from api
const initialState = {
	isLoading: true,
	isCategoryLoading: true,
	isContestModalActive: false,
	isContestCampaignModalActive: false,
	isCategorySelected: false,
	createContestLoading: false,
	isDayActive: false,
	isOtpVerified: true,
	verifyingOtp: false,
	date: moment(),
	showtime: moment(),
	selectedCategory: "",
	contestForCampaign: {},
	columns: [
		{
			title: "Name",
			dataIndex: "name",
			key: "name"
		},
		{
			title: "Title",
			dataIndex: "title",
			key: "age"
		},
		{
			title: "Start Time",
			dataIndex: "startTime",
			key: "start_time",
			defaultSortOrder: "descend",
			sorter: (a, b) => a.start_time - b.start_time
		}
	],
	contests: [],
	defaults: [],
	foreigns: {},
	types: []
};
function checkAccessToken() {
	try {
		const serializedTokens = localStorage.getItem("tokens");
		if (serializedTokens === null) {
			return false;
		}
		const access_token = JSON.parse(serializedTokens)["access_token"];
		const expires_in = JSON.parse(serializedTokens)["expires_in"];
		return access_token !== null && expires_in > 0;
	} catch (error) {
		return false;
	}
}

const Contests = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ALTER_CONTEST_MODAL:
			return {
				...state,
				isContestModalActive: action.payload
			};
		case actionTypes.ALTER_CONTEST_CAMPAIGN_MODAL:
			return {
				...state,
				isContestCampaignModalActive: action.payload
			};
		case actionTypes.SET_CONTEST_FOR_CAMPAIGN:
			return {
				...state,
				contestForCampaign: action.payload
			};
		case actionTypes.SET_NORMAL_LOCO:
			let { defaults, selectedCategory, showtime } = state;
			const isDayActive = action.payload;
			if (!isDayActive) {
				defaults[selectedCategory.name]["time"] = "22:00:00";
				defaults[selectedCategory.name]["prize"] = "100000";
				showtime = moment(defaults[selectedCategory.name]["time"], "HH:mm:ss");
			} else {
				defaults[selectedCategory.name]["time"] = "13:30:00";
				defaults[selectedCategory.name]["prize"] = "50000";
				showtime = moment(defaults[selectedCategory.name]["time"], "HH:mm:ss");
			}
			return {
				...state,
				defaults: defaults,
				isDayActive: action.payload,
				showtime: showtime
			};

		case actionTypes.POPULATE_CONTESTS:
			return {
				...state,
				contests: action.payload
			};
		case actionTypes.POPULATE_CONTEST_FOREIGNS:
			return {
				...state,
				foreigns: action.payload
			};
		case actionTypes.SET_CONTEST_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		case actionTypes.SET_CONTEST_CATEGORY_LOADING:
			return {
				...state,
				isCategoryLoading: action.payload
			};
		case actionTypes.POPULATE_TYPES:
			return {
				...state,
				types: action.payload
			};
		case actionTypes.POPULATE_CONTEST_DEFAULTS:
			return {
				...state,
				defaults: action.payload
			};
		case actionTypes.SET_CONTEST_CATEGORY:
			return {
				...state,
				isCategorySelected: true,
				selectedCategory: action.payload
			};
		case actionTypes.UPDATE_DATE:
			return {
				...state,
				date: action.payload
			};
		case actionTypes.UPDATE_SHOW_TIME:
			return {
				...state,
				showtime: action.payload
			};
		case actionTypes.RESET_CATEGORY_SELECTED:
			return {
				...state,
				isCategorySelected: false,
				selectedCategory: {}
			};
		case actionTypes.SET_CREATE_CONTEST_LOADING:
			return {
				...state,
				createContestLoading: action.payload
			};
		case actionTypes.SET_OTP_VERIFYING:
			return {
				...state,
				verifyingOtp: action.payload
			};
		case actionTypes.UPDATE_TOKENS:
			localStorage.setItem("tokens", JSON.stringify(action.payload));
			return {
				...state,
				verifyingOtp: false,
				isOtpVerified: true,
				tokens: action.payload
			};
		case actionTypes.CONTEST_CREATE_SUCCESS:
			return {
				...state,
				isContestModalActive: false,
				isCategorySelected: false
			};
		default:
			return state;
	}
};

export default Contests;
