import React, { Component } from "react";
import { connect } from "react-redux";
import {
	Form,
	Icon,
	Input,
	Button,
	Col,
	Row,
	TimePicker,
	DatePicker,
	Select,
	Switch,
	Card
} from "antd";
import {
	setCategoryLoading,
	updateDate,
	createNewContest,
	resetCategorySelected,
	setContestLoading,
	setNormalLoco
} from "../../actions/contests";
import moment from "moment";
import Category from "./Category";
import "./index.css";

const FormItem = Form.Item;
const Option = Select.Option;

function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.splitName = function() {
	return this.includes("_")
		? this.split("_")
				.map(i => i.capitalize())
				.join(" ")
		: this.includes("-")
		? this.split("-")
				.map(i => i.capitalize())
				.join(" ")
		: this;
};
String.prototype.splitTitle = function() {
	return this.includes("_")
		? this.split("_")
				.map(i => i.capitalize())
				.join("")
		: this.includes("-")
		? this.split("-")
				.map(i => i.capitalize())
				.join("")
		: this;
};

class ContestForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showtime: this.props.Contests.showtime
		};
		this.onChangeDate = this.onChangeDate.bind(this);
	}

	//WARNING! To be deprecated in React v17. Use new lifecycle static getDerivedStateFromProps instead.
	static getDerivedStateFromProps(nextProps, state) {
		if (!state.showtime.isSame(nextProps.Contests.showtime)) {
			return { showtime: nextProps.Contests.showtime };
		}
	}
	componentDidUpdate(prevProps, prevState) {
		console.log("cdu");
	}

	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				this.props.setContestLoading(true);
				let epochValues = Object.keys(values).reduce((result, key) => {
					if (values[key] instanceof moment) {
						let time = values[key];
						let current_date = moment(this.props.Contests.date).format(
							"YYYY-MM-DD"
						);
						let dateToChange = moment(time).format("HH:mm:ss");
						let newDate = moment(
							current_date + " " + dateToChange,
							"YYYY-MM-DD HH:mm:ss"
						);
						result[key] = newDate.valueOf();
					} else if (values[key] == undefined) {
						result[key] = null;
					} else result[key] = values[key];
					return result;
				}, {});
				await this.props.createNewContest(epochValues);
				this.props.setContestLoading(false);
			}
		});
	};
	onChangeDate(date) {
		if (date == null || date == undefined) return;
		this.props.updateDate(date);
	}
	resetCategorySelected = () => {
		this.props.resetCategorySelected();
	};
	render() {
		let {
			contests,
			types,
			isCategorySelected,
			selectedCategory,
			defaults,
			date,
			createContestLoading,
			isDayActive
		} = this.props.Contests;

		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		const that = this;
		const getFormComponent = item => {
			const type = this.props.Contests.types[item];
			if (
				[
					"language_cohort",
					"contest_type_uid",
					"contest_category_name"
				].includes(item)
			) {
				return (
					<Select
						showSearch
						style={{ width: 200 }}
						placeholder={`Select a ${item}`}
						optionFilterProp="children"
						filterOption={(input, option) =>
							option.props.children
								.toLowerCase()
								.indexOf(input.toLowerCase()) >= 0
						}
					>
						{this.props.Contests.foreigns[item]
							.filter(f => f.is_active)
							.map(f => (
								<Option value={f.name}>{f.name}</Option>
							))}
					</Select>
				);
			}
			switch (type) {
				case "DateTimeField":
					return <TimePicker format="HH:mm:ss" style={{ margin: "4%" }} />;
				case "BooleanField":
					return (
						<Switch
							checkedChildren={<Icon type="check" />}
							unCheckedChildren={<Icon type="close" />}
							style={{ marginLeft: "10px" }}
						/>
					);
				default:
					return <Input placeholder={item} />;
			}
		};
		const getCustomDefaultValue = item => {
			let name;
			let title = selectedCategory.name;
			if (selectedCategory.name == "normal-loco") {
				title = isDayActive ? "Day" : "Night";
				name = "loco-quiz";
			} else name = selectedCategory.name;
			switch (item) {
				case "title":
					return date.clone().format("MMM_DD_") + title.splitTitle();
				case "name":
					return "Next Game: " + name.splitName();
			}
		};

		return (
			<div>
				{!isCategorySelected || defaults.length == 0 ? (
					<Category />
				) : (
					<Form layout="inline" onSubmit={this.handleSubmit}>
						{/* {selectedCategory.name == "normal-loco" && (
							<Row type="flex" justify="space-between">
								<Col span={8} style={{ margin: "10px" }}>
									<Switch
										defaultChecked={isDayActive}
										onChange={checked => this.props.setNormalLoco(checked)}
										checkedChildren={"Day"}
										unCheckedChildren={"Night"}
										style={{ marginLeft: "10px" }}
									/>
								</Col>
							</Row>
						)} */}
						<Row type="flex" justify="space-between">
							<Col span={8}>
								<FormItem label={"user  "}>
									{getFieldDecorator("user", {
										initialValue: defaults[selectedCategory.name]["user"],
										rules: [{ required: true }]
									})(<Input disabled placeholder={"user"} />)}
								</FormItem>
								<Row>
									{Object.keys(contests[0])
										.filter(
											item =>
												types[item] !== "BooleanField" &&
												types[item] !== "DateTimeField" &&
												item !== "uid"
										)
										.sort((a, b) => a.length - b.length)
										.map(item => (
											<Col span={24} style={{ margin: "10px" }}>
												<FormItem
													label={item}
													validateStatus={
														isFieldTouched(item) && getFieldError(item)
															? "error"
															: ""
													}
													help={
														(isFieldTouched(item) && getFieldError(item)) || ""
													}
												>
													{getFieldDecorator(item, {
														initialValue:
															defaults[selectedCategory.name][item] ||
															getCustomDefaultValue(item),
														rules: [
															{
																required: item !== "language_cohort" && true,
																message: `Please input ${item}`
															}
														]
													})(getFormComponent(item))}
												</FormItem>
											</Col>
										))}
								</Row>
							</Col>

							<Col span={8}>
								<Row>
									<Card style={{ width: "auto" }}>
										<Col span={24} style={{ margin: "10px" }}>
											<FormItem
												label="Date"
												validateStatus={
													isFieldTouched("Date") && getFieldError("Date")
														? "error"
														: ""
												}
												help={
													(isFieldTouched("Date") && getFieldError("Date")) ||
													""
												}
											>
												{getFieldDecorator("date", {
													initialValue: date,
													rules: [
														{ required: true, message: `Please input Date` }
													]
												})(
													<DatePicker
														onChange={that.onChangeDate}
														format="YYYY-MM-DD"
														style={{ margin: "4%" }}
													/>
												)}
											</FormItem>
										</Col>

										{Object.keys(contests[0])
											.filter(item => types[item] === "DateTimeField")
											.map(item => (
												<Row>
													<Col span={18} style={{ margin: "10px" }}>
														<FormItem
															label={item}
															validateStatus={
																isFieldTouched(item) && getFieldError(item)
																	? "error"
																	: ""
															}
															help={
																(isFieldTouched(item) && getFieldError(item)) ||
																""
															}
														>
															{getFieldDecorator(item, {
																// getValueFromEvent:defaults[selectedCategory.name][item] == undefined ? this.state.showtime : (defaults[selectedCategory.name][item] > 0) ? this.state.showtime.clone().add(parseFloat(defaults[selectedCategory.name][item]), "minutes") : this.state.showtime.clone().subtract(Math.abs(defaults[selectedCategory.name][item]), "minutes"),
																initialValue:
																	defaults[selectedCategory.name][item] ==
																	undefined
																		? this.state.showtime
																		: defaults[selectedCategory.name][item] > 0
																		? this.state.showtime
																				.clone()
																				.add(
																					parseFloat(
																						defaults[selectedCategory.name][
																							item
																						]
																					),
																					"minutes"
																				)
																		: this.state.showtime
																				.clone()
																				.subtract(
																					Math.abs(
																						defaults[selectedCategory.name][
																							item
																						]
																					),
																					"minutes"
																				),
																rules: [
																	{
																		required: true,
																		message: `Please input ${item}`
																	}
																]
															})(getFormComponent(item))}
														</FormItem>
													</Col>
													<Col
														span={2}
														style={{ margin: "20px", marginLeft: "0px" }}
													/>
												</Row>
											))}
									</Card>
								</Row>
							</Col>

							<Col span={6}>
								<Row>
									<Card style={{ width: "auto" }}>
										{Object.keys(contests[0])
											.filter(item => types[item] === "BooleanField")
											.map(item => (
												<Col span={24} style={{ margin: "10px" }}>
													<FormItem
														label={item}
														validateStatus={
															isFieldTouched(item) && getFieldError(item)
																? "error"
																: ""
														}
														help={
															(isFieldTouched(item) && getFieldError(item)) ||
															""
														}
													>
														{getFieldDecorator(item, {
															valuePropName: "checked",
															initialValue:
																defaults[selectedCategory.name][item],
															rules: [
																{
																	required: true,
																	message: `Please input ${item}`
																}
															]
														})(getFormComponent(item))}
													</FormItem>
												</Col>
											))}
									</Card>
								</Row>
							</Col>
						</Row>
						<FormItem />
						<Row>
							<Col offset={12}>
								<Button
									type="primary"
									size="large"
									ghost
									style={{ fontSize: "1.3rem", width: "150px", height: "50px" }}
									onClick={this.resetCategorySelected}
									disabled={hasErrors(getFieldsError())}
								>
									{" "}
									Go Back{" "}
								</Button>
								<Button
									type="primary"
									size="large"
									style={{
										marginLeft: "20px",
										fontSize: "1.3rem",
										width: "150px",
										height: "50px"
									}}
									htmlType="submit"
									loading={createContestLoading}
									disabled={hasErrors(getFieldsError())}
								>
									{" "}
									Submit{" "}
								</Button>
							</Col>
						</Row>
					</Form>
				)}
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		setCategoryLoading: () => dispatch(setCategoryLoading(false)),
		updateDate: date => dispatch(updateDate(date.clone())),
		createNewContest: contest => dispatch(createNewContest(contest)),
		resetCategorySelected: () => dispatch(resetCategorySelected()),
		setContestLoading: bool => dispatch(setContestLoading(bool)),
		setNormalLoco: bool => dispatch(setNormalLoco(bool))
	};
};
const mapStateToProps = state => ({
	Contests: state.Contests
});

const WrappedContestForm = Form.create()(ContestForm);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedContestForm);
