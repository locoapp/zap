import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Card,Skeleton, Row, Col } from 'antd';

import {fetchContestForeigns,setCategoryLoading,setContestCategory,fetchContestDefaults} from '../../actions/contests'
import './index.css'

class Category  extends Component {


    async componentDidMount() {
        await this.props.fetchContestForeigns()
        await this.props.fetchContestDefaults()
    }

    selectContestCategory = (category) => {
        this.props.setContestCategory(category)

    }
    render() { 
        const {isCategoryLoading,foreigns} = this.props.Contests
        const categories = foreigns.contest_category_name
        return ( 
            <div>
                {
                    isCategoryLoading
                    ?
                    <Skeleton active />
                    :
                    <div>
                        <Row justify="center">
                        {

                           categories.map(category => 
                                <Col span={4} style={{margin:'1%'}} >
                                <Card
                                hoverable
                                onClick={ () => this.selectContestCategory(category)}
                                style={{ background:'white'}}
                                cover={ <img alt="example" src={category.card_image} />}
                              >
                              <h4 className="card_footer">{category.name}</h4>
                              </Card>
                              </Col>
                                )
                        }
                        </Row>
                   
                  </div>
                }
              
            </div>
         );
    }
}
const mapDispatchToProps = dispatch => {
  return {
    fetchContestForeigns : () => dispatch(fetchContestForeigns()),
    fetchContestDefaults : () => dispatch(fetchContestDefaults()),
    setCategoryLoading : () =>  dispatch(setCategoryLoading(false)),
    setContestCategory : (category) => dispatch(setContestCategory(category))
  }
}
const mapStateToProps = state => ({
    Contests:state.Contests
  
})
export default connect(mapStateToProps,mapDispatchToProps)(Category);