import React, { Component, Suspense } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Auth } from "aws-amplify";
import { ContestModal } from "../Modals/ContestModal";
import { ContestCampaignModal } from "../Modals/ContestCampaignModal";
import ContestCampaign from "../Campaigns/ContestCampaign";
import ContestForm from "./ContestForm";
import { Skeleton, Table, Checkbox, Button } from "antd";
import {
	fetchContests,
	setLoading,
	alterContestModal,
	checkContestCampaigns
} from "../../actions/contests";
import { alterContestCampaignModalActive, cleanTable } from "../../actions";
import "./index.css";

class Contests extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdmin: false,
			isCampaign: false,
			isContests: false
		};
	}

	componentWillUnmount() {
		this.setState({ isAdmin: false });
	}
	async componentDidMount() {
		try {
			let groups = (await Auth.currentSession()).idToken.payload[
				"cognito:groups"
			];
			let isAdmin = groups.indexOf("admin") > -1;
			let isCampaign = groups.indexOf("campaigns") > -1;
			let isContests = groups.indexOf("contests") > -1;
			this.setState({
				isAdmin: isAdmin,
				isCampaign: isCampaign,
				isContests: isContests
			});
		} catch (e) {
			this.setState({ isAdmin: false, isCampaign: false, isContests: false });
		}
		if (this.props.Contests.isOtpVerified) {
			await this.props.fetchContests();
			let { contests, columns } = this.props.Contests;
			this.columns = [
				...columns,
				{
					title: "Is Active",
					dataIndex: "is_active",
					render: (text, record) => {
						return <Checkbox checked={record.is_active} disabled={false} />;
					}
				},
				{
					title: "Prize Distributed",
					dataIndex: "is_prize_distributed",
					render: (text, record) => {
						return (
							<Checkbox
								checked={record.is_prize_distributed}
								disabled={false}
							/>
						);
					}
				}
			];
			if (this.state.isCampaign || this.state.isAdmin) {
				this.columns = [
					...this.columns,
					{
						title: "Notifications",
						dataIndex: "campaigns",
						render: (text, record) => {
							return (
								<Button
									size="small"
									type="primary"
									onClick={() => this.showContestCampaigns(record.key, record)}
								>
									Campaigns
								</Button>
							);
						}
					}
				];
			}
			this.dataSource = contests.map(contest => ({
				key: contest.uid,
				name: contest.name,
				title: contest.title,
				start_time: contest.start_time,
				startTime: moment(contest.start_time).format("MMM D, YYYY, hh:mm A  "),
				is_prize_distributed: contest.is_prize_distributed,
				is_active: contest.is_active
			}));

			this.props.setLoading(false);
		} else {
			this.props.setLoading(true);
		}
	}
	showContestCampaigns = (uid, contest) => {
		this.props.cleanTable();
		this.props.checkContestCampaigns(uid, contest);
	};
	showContestModal = () => {
		this.props.alterContestModal(true);
	};
	handleCancel = () => {
		this.props.alterContestModal(false);
	};
	handleCampaignCancel = () => {
		this.props.alterContestCampaignModalActive(false);
	};

	render() {
		const {
			isLoading,
			isContestModalActive,
			isOtpVerified,
			isContestCampaignModalActive,
			contestForCampaign
		} = this.props.Contests;
		return (
			<div>
				{isLoading ? (
					<Skeleton active />
				) : isOtpVerified ? (
					<div style={{ margin: "0.1%", background: "#f5f6fa" }}>
						{(this.state.isAdmin || this.state.isContests) && (
							<Button
								size="large"
								type="primary"
								onClick={this.showContestModal}
								style={{ margin: "30px" }}
							>
								ADD CONTEST
							</Button>
						)}
						<ContestModal
							title="CONTEST"
							visible={isContestModalActive}
							handleCancel={this.handleCancel}
						>
							<ContestForm />
						</ContestModal>
						<ContestCampaignModal
							title={contestForCampaign.name}
							visible={isContestCampaignModalActive}
							handleCancel={this.handleCampaignCancel}
						>
							<ContestCampaign
								uid={contestForCampaign.key}
								is_prize_distributed={contestForCampaign.is_prize_distributed}
								start_time={contestForCampaign.start_time}
							/>
						</ContestCampaignModal>
						<Suspense fallback={<Skeleton />}>
							<Table dataSource={this.dataSource} columns={this.columns} />
						</Suspense>
					</div>
				) : (
					<div />
				)}
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		fetchContests: () => dispatch(fetchContests()),
		setLoading: () => dispatch(setLoading(false)),
		alterContestModal: bool => dispatch(alterContestModal(bool)),
		alterContestCampaignModalActive: bool =>
			dispatch(alterContestCampaignModalActive(bool)),
		checkContestCampaigns: (uid, contest) =>
			dispatch(checkContestCampaigns(uid, contest)),
		cleanTable: () => dispatch(cleanTable())
	};
};
const mapStateToProps = state => ({
	Contests: state.Contests
});
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Contests);
