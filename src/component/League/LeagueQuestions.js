import React, { Component } from "react";
import { connect } from "react-redux";
import {
	Skeleton,
	Carousel,
	Button,
	Icon,
	Select,
	Form,
	Input,
	InputNumber
} from "antd";
import {
	fetchLeagueQuestions,
	setQuestionLoading,
	uploadQuestions,
	uploadAnswers,
	startPrizeDistribution
} from "../../actions/league";
import AddQuestions from "./AddQuestions";
import "./index.css";
import QuestionsPreview from "./QuestionsPreview";
const Option = Select.Option;
const FormItem = Form.Item;

class LeagueQuestions extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	handleSubmit = async () => {
		let game_uid = this.props.League.open_league.key;
		let questions = this.props.League.currentQuestions;
		await this.props.uploadQuestions(game_uid, questions);
	};
	submitAnswers = async () => {
		await this.props.uploadAnswers();
	};
	startPrizeDistribution = async () => {
		await this.props.startPrizeDistribution();
	};

	async componentDidMount() {
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		// document.getElementsByClassName(
		// 	"ant-select-selection__rendered"
		// )[0].style.background = "#1890ff";
		const { open_league } = this.props.League;
		await this.props.fetchLeagueQuestions(open_league.key);
		this.props.setQuestionLoading(false);
	}
	async componentDidUpdate(prevProps) {
		const { open_league } = this.props.League;
		if (open_league.key !== prevProps.League.open_league.key) {
			this.props.alterIsFileLocal(false);
			this.props.setQuestionLoading(true);
			await this.props.fetchLeagueQuestions(open_league.key);
			this.props.setQuestionLoading(false);
		}
	}
	onChange(a, b, c) {
		console.log(a, b, c);
	}
	getStyle = (answer, uid) => {
		let isCorrect = uid == answer;
		if (isCorrect) {
			return {
				background: `#00bc5f`
			};
		} else
			return {
				background: ` #e7e5e7 `
			};
	};
	changePreviewLanguage = value => this.props.changePreviewLanguage(value);
	onAnswerInputChange = value => {
		this.props.updateAnswerInput(value, "kk");
	};
	render() {
		const {
			currentQuestions,
			open_league,
			status,
			isFileLocal,
			isQuestionUploading,
			isAnswersUploading,
			defaultLanguage,
			languages,
			answers,
			areAnswersThere,
			isLockingLoading
		} = this.props.League;
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		let isAnswerInputActive =
			currentQuestions.length > 0 && open_league.status == status.GO;

		return (
			<div>
				{this.props.League.isQuestionsLoading ? (
					<Skeleton active />
				) : (
					<div>
						{currentQuestions.length != 0 && (
							<Select
								defaultValue={defaultLanguage}
								style={{
									width: 120,
									borderRadius: "2px"
								}}
								onChange={this.changePreviewLanguage}
							>
								{Object.keys(languages).map(lang => (
									<Option value={lang}>{lang}</Option>
								))}
							</Select>
						)}
						<Carousel afterChange={this.onChange}>
							{currentQuestions.length == 0 ? (
								<div className="game_question_container">
									<h1 className="game_question_text">
										{open_league.name.toUpperCase()}
									</h1>
									<AddQuestions />
								</div>
							) : (
								currentQuestions.map(question => (
									<QuestionsPreview question={question}>
										{open_league.status == status.GO &&
											!isFileLocal &&
											currentQuestions.length > 0 && (
												<div>
													{parseInt(question.type) == 2 ? (
														<Input
															addonBefore={<Icon type="money-collect" />}
															placeholder="Enter Answer"
															className=" game_fix game_prize"
															type="number"
															min="1"
															onChange={e =>
																this.props.updateAnswerInput(
																	parseInt(e.target.value),
																	question.uid
																)
															}
														/>
													) : (
														<Select
															placeholder="Select an answer"
															onChange={value =>
																this.props.updateAnswerOption(
																	value,
																	question.uid
																)
															}
															className=" answer_fix"
														>
															{Object.keys(question.options).map(uid => (
																<Option value={uid}>
																	{question.options[uid].text[1]}
																</Option>
															))}
														</Select>
													)}
												</div>
											)}
									</QuestionsPreview>
								))
							)}
						</Carousel>
						{console.log(open_league.status)}
						{open_league.status == status.GO &&
							currentQuestions.length > 0 &&
							areAnswersThere && (
								<div style={{ display: "flex", flexDirection: "row-reverse" }}>
									<div
										className="league_save_progress"
										style={{ margin: "10px" }}
										onClick={this.startPrizeDistribution}
									>
										<p style={{ margin: "0px", paddingLeft: "0px" }}>LOCK</p>
										{isLockingLoading && <Icon type="loading" />}
									</div>
								</div>
							)}
						{open_league.status == status.GO &&
							currentQuestions.length > 0 &&
							Object.keys(answers).length == currentQuestions.length && (
								<div style={{ display: "flex", flexDirection: "row-reverse" }}>
									<div
										className="league_save_progress"
										style={{ margin: "10px" }}
										onClick={this.submitAnswers}
									>
										<p style={{ margin: "0px", paddingLeft: "10px" }}>UPDATE</p>
										{isAnswersUploading && <Icon type="loading" />}
									</div>
								</div>
							)}
						{open_league.status == status.GO &&
							currentQuestions.length > 0 &&
							isFileLocal && (
								<div style={{ display: "flex", flexDirection: "row-reverse" }}>
									<div
										className="league_save_progress"
										onClick={this.handleSubmit}
									>
										<p style={{ margin: "0px", paddingLeft: "10px" }}>UPLOAD</p>
										{isQuestionUploading && <Icon type="loading" />}
									</div>
								</div>
							)}
					</div>
				)}
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		fetchLeagueQuestions: game_uid => dispatch(fetchLeagueQuestions(game_uid)),
		setQuestionLoading: bool => dispatch(setQuestionLoading(bool)),
		alterIsFileLocal: bool =>
			dispatch({ type: "alter_is_file_local", payload: bool }),
		uploadQuestions: (game_uid, questions) =>
			dispatch(uploadQuestions(game_uid, questions)),
		changePreviewLanguage: lang =>
			dispatch({ type: "change_preview_language", payload: lang }),
		uploadAnswers: () => dispatch(uploadAnswers()),
		startPrizeDistribution: () => dispatch(startPrizeDistribution()),
		updateAnswerOption: (value, question_uid) => {
			dispatch({
				type: "update_answer_option",
				payload: {
					question_uid: question_uid,
					answer_option: value
				}
			});
		},
		updateAnswerInput: (value, question_uid) => {
			dispatch({
				type: "update_answer_input",
				payload: {
					question_uid: question_uid,
					answer_input: value
				}
			});
		}
	};
};
const mapStateToProps = state => ({
	League: state.League
});
const WrappedAnswers = Form.create()(LeagueQuestions);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedAnswers);
