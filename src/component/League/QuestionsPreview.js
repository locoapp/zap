import React, { Component } from "react";
import { connect } from "react-redux";
import { Skeleton, Carousel, Button, Select } from "antd";
import { uploadQuestions } from "../../actions/league";
import AddQuestions from "./AddQuestions";
import "./index.css";

const Option = Select.Option;

class QuestionsPreview extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	onChange(a, b, c) {
		console.log(a, b, c);
	}
	componentDidMount() {
		// document.getElementsByClassName(".slick-slide").style.height = "100%";
	}
	getStyle = (answer, uid, updated_answer) => {
		let isCorrect = uid == answer;
		if (updated_answer != undefined)
			isCorrect = updated_answer.answer_option == uid;
		if (isCorrect) {
			return {
				background: `#00bc5f`
			};
		} else
			return {
				background: ` #e7e5e7 `
			};
	};

	render() {
		let {
			question,
			League: { open_league, status, languages, defaultLanguage, answers }
		} = this.props;
		return (
			<div>
				<div className="game_question_container">
					<h1 className="game_question_text">
						{`Q${question.order}.  ${
							question.text[languages[defaultLanguage]]
						}  `}
					</h1>
					{+question.type == 1
						? Object.keys(question.options).map(uid => {
								return (
									<div
										className="game_question_option"
										style={this.getStyle(
											question.answer_option,
											uid,
											answers[question.uid]
										)}
									>
										<div className="game_question_option_text">
											{question.options[uid].text[languages[defaultLanguage]]}
										</div>
										<div className="game_question_count_text">
											{question.type == 1
												? question.option_statistics[uid]
												: ""}
										</div>
									</div>
								);
						  })
						: (question.answer_input || answers[question.uid]) && (
								<div className="game_question_input">
									{question.answer_input || answers[question.uid].answer_input}
								</div>
						  )}

					<div className="answer_input">{this.props.children}</div>
				</div>
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {};
};
const mapStateToProps = state => ({
	League: state.League
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(QuestionsPreview);
