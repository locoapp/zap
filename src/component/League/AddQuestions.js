import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Icon, Form, Button, Upload, message, Modal } from "antd";
import { updateLeagueQuestions } from "../../actions/league";
import "./create_league.css";

const FormItem = Form.Item;

function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}

function CSVToArray(strData, strDelimiter) {
	// Check to see if the delimiter is defined. If not,
	// then default to comma.
	strDelimiter = strDelimiter || ",";

	// Create a regular expression to parse the CSV values.
	var objPattern = new RegExp(
		// Delimiters.
		"(\\" +
			strDelimiter +
			"|\\r?\\n|\\r|^)" +
			// Quoted fields.
			'(?:"([^"]*(?:""[^"]*)*)"|' +
			// Standard fields.
			'([^"\\' +
			strDelimiter +
			"\\r\\n]*))",
		"gi"
	);

	// Create an array to hold our data. Give the array
	// a default empty first row.
	var arrData = [[]];

	// Create an array to hold our individual pattern
	// matching groups.
	var arrMatches = null;

	// Keep looping over the regular expression matches
	// until we can no longer find a match.
	while ((arrMatches = objPattern.exec(strData))) {
		// Get the delimiter that was found.
		var strMatchedDelimiter = arrMatches[1];

		// Check to see if the given delimiter has a length
		// (is not the start of string) and if it matches
		// field delimiter. If id does not, then we know
		// that this delimiter is a row delimiter.
		if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
			// Since we have reached a new row of data,
			// add an empty row to our data array.
			arrData.push([]);
		}

		var strMatchedValue;

		// Now that we have our delimiter out of the way,
		// let's check to see which kind of value we
		// captured (quoted or unquoted).
		if (arrMatches[2]) {
			// We found a quoted value. When we capture
			// this value, unescape any double quotes.
			strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
		} else {
			// We found a non-quoted value.
			strMatchedValue = arrMatches[3];
		}

		// Now that we have our value string, let's add
		// it to the data array.
		arrData[arrData.length - 1].push(strMatchedValue);
	}

	// Return the parsed data.
	return arrData.slice(1, arrData.length);
}
function generateUID() {
	var firstPart: any = (Math.random() * 46656) | 0;
	var secondPart: any = (Math.random() * 46656) | 0;
	var lastPart: any = (Math.random() * 46656) | 0;
	firstPart = ("000" + firstPart.toString(36)).slice(-3);
	secondPart = ("000" + secondPart.toString(36)).slice(-3);
	lastPart = ("000" + lastPart.toString(36)).slice(-4);
	return (firstPart + secondPart + lastPart).toString().toUpperCase();
}
function formatQuestions(json, map) {
	let last_index = -1;
	let last_options_uid = [];
	return json.reduce((a, b, i) => {
		let type = b[3] == 0 && b[4] == 0 && b[5] == 0 ? 2 : 1;
		//empty field 1 i.e. question rank
		if (b[0] == 0) {
			if (type == 1)
				b.slice(3, 6).map((option, index) => {
					if (!b[index + 3] == 0)
						a[last_index].options[last_options_uid[index]].text[map[b[1]]] =
							b[index + 3];
				});
			a[last_index].trivia[map[b[1]]] = b[6] || b[b.length - 1];
			a[last_index].text[map[b[1]]] = b[2];
		} else {
			last_index += 1;
			last_options_uid = [];
			a[last_index] = {
				order: last_index + 1,
				text: {},
				trivia: {},
				type: type,
				coins: parseInt(b[7])
			};
			if (type == 1) {
				a[last_index].options = {};
				a[last_index].option_statistics = {};
				b.slice(3, 6).map((option, index) => {
					if (!b[index + 3] == 0) {
						last_options_uid.push(generateUID());
						a[last_index].option_statistics[last_options_uid[index]] = 0;
						a[last_index].options[last_options_uid[index]] = {
							rank: index + 1,
							text: {
								[map[b[1]]]: b[index + 3]
							}
						};
					}
				});
			}
			a[last_index].trivia[map[b[1]]] = b[6];

			a[last_index].text[map[b[1]]] = b[2];
		}
		return a;
	}, []);
}

class AddQuestions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fileList: []
		};
	}
	componentDidMount() {
		document.getElementsByClassName("ant-modal-body")[0].style.padding = "0px";
		document.getElementsByClassName("ant-modal-body")[0].style.backgroundColor =
			"#f6f7fa";
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		let elements = document.getElementsByClassName("ant-form-item");
		for (let ele of elements) {
			ele.style.display = "flex";
			ele.style.flexDirection = "column";
		}
		let items = document.getElementsByClassName("ant-form-item-label");
		for (let it of items) it.style.textAlign = "left";
	}
	handleSubmit = async e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				let questions = await new Response(values.questions.file).text();
				let q_array = CSVToArray(questions.toString());
				let all_questions = formatQuestions(
					q_array,
					this.props.League.languages
				);
				await this.props.alterIsFileLocal(true);
				await this.props.updateLeagueQuestions(all_questions);
			}
		});
	};

	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		const { fileList } = this.state;

		const props = {
			onRemove: file => {
				this.setState(state => {
					const index = state.fileList.indexOf(file);
					const newFileList = state.fileList.slice();
					newFileList.splice(index, 1);
					return {
						fileList: newFileList
					};
				});
			},
			beforeUpload: file => {
				const isCSV = file.type === "text/csv";
				if (!isCSV) {
					message.error("You can only upload CSV file!");
					return false;
				}
				this.setState(state => ({
					fileList: [...state.fileList, file]
				}));
				return false;
			},
			fileList
		};

		return (
			<Form layout="inline" onSubmit={this.handleSubmit}>
				<div className="league_container">
					<div
						className="league_content"
						style={{
							display: "flex",
							flexDirection: "column",
							alignItems: "center"
						}}
					>
						<div className="game_container">
							<FormItem style={{ width: "100%" }}>
								{getFieldDecorator("questions", {
									rules: [{ required: true }]
								})(
									<Upload {...props}>
										{" "}
										{fileList.length < 1 && (
											<div className="upload_border">
												<div
													className="league_save_progress"
													style={{
														background: "transparent",
														color: "#4a58a3"
													}}
												>
													<p style={{ margin: "10px" }}>Select File</p>
													<Icon
														style={{ fontSize: "2rem" }}
														type="cloud-upload"
													/>
												</div>
											</div>
										)}
									</Upload>
								)}
							</FormItem>
						</div>
						{fileList.length >= 1 && (
							<div className="league_save_progress" onClick={this.handleSubmit}>
								<p style={{ margin: "0px", paddingLeft: "10px" }}>Preview</p>
							</div>
						)}
					</div>
				</div>
			</Form>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		updateLeagueQuestions: values => dispatch(updateLeagueQuestions(values)),
		alterIsFileLocal: bool =>
			dispatch({ type: "alter_is_file_local", payload: bool })
	};
};
const mapStateToProps = state => ({
	League: state.League
});
const WrappedAddQuestions = Form.create()(AddQuestions);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedAddQuestions);
