import React from "react";
import { connect } from "react-redux";
import { Auth } from "aws-amplify";
import {
	setLoading,
	fetchLeagues,
	alterLeagueModal,
	setCurrentLeague,
	alterCreateLeagueModal,
	getNextLeagues
} from "../../actions/league";
import { Skeleton, Button, Table, Pagination } from "antd";
import LeagueModal from "../Modals/LeagueModal";
import CreateLeagueModal from "../Modals/CreateLeagueModal";
import CreateLeague from "./CreateLeague";
import LeagueQuestions from "./LeagueQuestions";

import moment from "moment";

class League extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLeagueAuthenticated: false,
			currentPage: 1
		};
	}
	async componentDidMount() {
		try {
			let groups = (await Auth.currentSession()).idToken.payload[
				"cognito:groups"
			];
			let isLeagueAuthenticated =
				groups.indexOf("admin") > -1 || groups.indexOf("league") > -1;
			this.setState({ isLeagueAuthenticated: isLeagueAuthenticated });
		} catch (e) {
			this.setState({ isLeagueAuthenticated: false });
		}
		await this.props.fetchLeagues();
		let columns = this.props.leagueColumns;
		let { leagues } = this.props.League;
		this.columns = [
			{
				title: "Name",
				dataIndex: "name",
				render: (text, record) => {
					return <div>{record.name}</div>;
				}
			},

			...columns
		];
		if (this.state.isLeagueAuthenticated) {
			this.columns = [
				...this.columns,
				{
					title: "Edit",
					dataIndex: "edit",
					render: (text, record) => {
						return (
							<Button
								size="small"
								type="primary"
								onClick={() => this.alterLeagueModal(record)}
							>
								Edit
							</Button>
						);
					}
				}
			];
		}

		this.props.setLoading(false);
	}
	alterLeagueModal = record => {
		this.props.setCurrentLeague(record);
		this.props.alterLeagueModal(true);
	};

	handleCancel = () => {
		this.props.alterLeagueModal(false);
	};
	handleCreateLeagueCancel = () => {
		this.props.alterCreateLeagueModal(false);
	};
	showNewLeagueModal = () => {
		this.props.alterCreateLeagueModal(true);
	};
	onPageChange = page => {
		this.setState({ currentPage: page });
		this.props.getNextLeagues();
	};

	render() {
		const { isLoading } = this.props.League;
		// let Pagination = <Pagination current={1} total={50} simple={true} />;
		return (
			<div>
				{isLoading ? (
					<Skeleton active />
				) : (
					<div>
						{this.state.isLeagueAuthenticated && (
							<Button
								size="large"
								type="primary"
								onClick={this.showNewLeagueModal}
								style={{ margin: "30px" }}
							>
								ADD LEAGUE
							</Button>
						)}

						<Table
							dataSource={this.props.League.dataSource}
							columns={this.columns}
							pagination={{
								current: this.state.currentPage,
								total: 100,
								simple: true,
								position: "top",
								onChange: this.onPageChange
							}}
						/>
						<CreateLeagueModal
							handleCancel={this.handleCreateLeagueCancel}
							visible={this.props.League.isCreateLeagueModalActive}
						>
							<CreateLeague />
						</CreateLeagueModal>
						<LeagueModal
							handleCancel={this.handleCancel}
							visible={this.props.League.isLeagueModalActive}
						>
							<LeagueQuestions />
						</LeagueModal>
					</div>
				)}
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getNextLeagues: () => dispatch(getNextLeagues()),
		setCurrentLeague: league => {
			dispatch(setCurrentLeague(league));
		},
		alterLeagueModal: bool => {
			dispatch(alterLeagueModal(bool));
		},
		alterCreateLeagueModal: bool => dispatch(alterCreateLeagueModal(bool)),
		setLoading: bool => dispatch(setLoading(bool)),
		fetchLeagues: () => dispatch(fetchLeagues())
	};
};

const mapStateToProps = state => {
	return {
		League: state.League,
		leagueColumns: state.Table.leagueColumns
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(League);
