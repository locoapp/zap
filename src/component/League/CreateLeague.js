import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Icon, Form, Button, Upload, message, Modal } from "antd";
import { Storage } from "aws-amplify";
import DateTimePicker from "react-widgets/lib/DateTimePicker";
import {
	updateLeagueGame,
	setCreateLeagueLoading,
	s3Upload
} from "../../actions/league";
import SuccessModal from "./SuccessModal";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";

import "./create_league.css";
import "react-widgets/dist/css/react-widgets.css";

const FormItem = Form.Item;
Moment.locale("en");
momentLocalizer();

function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}

function getBase64(img, callback) {
	const reader = new FileReader();
	reader.addEventListener("load", () => callback(reader.result));
	reader.readAsText(img);
}

function CSVToArray(strData, strDelimiter) {
	// Check to see if the delimiter is defined. If not,
	// then default to comma.
	strDelimiter = strDelimiter || ",";

	// Create a regular expression to parse the CSV values.
	var objPattern = new RegExp(
		// Delimiters.
		"(\\" +
			strDelimiter +
			"|\\r?\\n|\\r|^)" +
			// Quoted fields.
			'(?:"([^"]*(?:""[^"]*)*)"|' +
			// Standard fields.
			'([^"\\' +
			strDelimiter +
			"\\r\\n]*))",
		"gi"
	);

	// Create an array to hold our data. Give the array
	// a default empty first row.
	var arrData = [[]];

	// Create an array to hold our individual pattern
	// matching groups.
	var arrMatches = null;

	// Keep looping over the regular expression matches
	// until we can no longer find a match.
	while ((arrMatches = objPattern.exec(strData))) {
		// Get the delimiter that was found.
		var strMatchedDelimiter = arrMatches[1];

		// Check to see if the given delimiter has a length
		// (is not the start of string) and if it matches
		// field delimiter. If id does not, then we know
		// that this delimiter is a row delimiter.
		if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
			// Since we have reached a new row of data,
			// add an empty row to our data array.
			arrData.push([]);
		}

		var strMatchedValue;

		// Now that we have our delimiter out of the way,
		// let's check to see which kind of value we
		// captured (quoted or unquoted).
		if (arrMatches[2]) {
			// We found a quoted value. When we capture
			// this value, unescape any double quotes.
			strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
		} else {
			// We found a non-quoted value.
			strMatchedValue = arrMatches[3];
		}

		// Now that we have our value string, let's add
		// it to the data array.
		arrData[arrData.length - 1].push(strMatchedValue);
	}

	// Return the parsed data.
	return arrData.slice(1, arrData.length);
}
function formatDescription(json, map) {
	return json.reduce((a, b) => {
		a[map[b[0]]] = b[1];
		return a;
	}, {});
}
let structure = {
	order: 1
};
function generateUID() {
	var firstPart: any = (Math.random() * 46656) | 0;
	var secondPart: any = (Math.random() * 46656) | 0;
	var lastPart: any = (Math.random() * 46656) | 0;
	firstPart = ("000" + firstPart.toString(36)).slice(-3);
	secondPart = ("000" + secondPart.toString(36)).slice(-3);
	lastPart = ("000" + lastPart.toString(36)).slice(-4);
	return (firstPart + secondPart + lastPart).toString().toUpperCase();
}

function formatRankPrize(json) {
	let rank_prize = [];
	json.map(item => {
		let toInsert = new Array(parseInt(item[1]) - parseInt(item[0]) + 1).fill(
			parseInt(item[2])
		);
		rank_prize.push(...toInsert);
	});
	return rank_prize;
}

class CreateLeague extends Component {
	constructor(props) {
		super(props);
		this.state = {
			step: 1,
			totalSteps: 3,
			fileList: [],
			descfileList: [],
			prizeFileList: [],
			ruleFileList: {
				Hindi: [],
				English: [],
				Marathi: [],
				Bengali: [],
				Telugu: [],
				Tamil: []
			}
		};
	}
	componentDidMount() {
		document.getElementsByClassName("ant-modal-body")[0].style.padding = "0px";
		document.getElementsByClassName("ant-modal-body")[0].style.backgroundColor =
			"#f6f7fa";
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		let elements = document.getElementsByClassName("ant-form-item");
		for (let ele of elements) {
			ele.style.display = "flex";
			ele.style.flexDirection = "column";
		}
		let items = document.getElementsByClassName("ant-form-item-label");
		for (let it of items) it.style.textAlign = "left";
	}
	s3RuleUpload = async file => {
		const filename = `${Date.now()}-${file.name}`;

		const stored = await Storage.put(filename, file, {
			contentType: file.type
		});
		let res = await Storage.get(stored.key, { level: "public" });
		let file_url = res.split("?")[0];
		let cdn_url = "https://static.getloconow.com" + file_url.split(".com")[1];
		return cdn_url;
	};
	handleSubmit = async e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				this.props.setCreateLeagueLoading(true);
				let description = await new Response(values.description.file).text();
				let rank_prize = await new Response(values.rank_prize.file).text();
				const imagefile = values.image_url.file;
				let rule = await Object.keys(this.props.League.languages).reduce(
					async (prev_promise, lang) => {
						let rules = await prev_promise;
						rules[this.props.League.languages[lang]] = await this.s3RuleUpload(
							values[lang].file
						);
						return rules;
					},
					Promise.resolve({})
				);
				await this.props.s3Upload(imagefile);
				let { stored_image } = this.props.League;
				let new_values = Object.assign(values, {});
				new_values.starts_at = new Date(values.starts_at).getTime();
				new_values.ends_at = new Date(values.ends_at).getTime();
				new_values.rule = rule;
				new_values.result_announced_at = new Date(
					values.result_announced_at
				).getTime();
				new_values.image_url = stored_image;
				if (!stored_image) {
					this.props.setCreateLeagueLoading(false);
					message.error("Image Upload error ! retry ");
				}
				new_values.rank_prize = formatRankPrize(
					CSVToArray(rank_prize.toString())
				);
				let rank_total_prize = new_values.rank_prize.reduce((a, b) => a + b, 0);
				if (rank_total_prize != new_values.prize_money) {
					this.props.setCreateLeagueLoading(false);

					message.error("Prize money is not equal to rank total prize ");
					return;
				}
				new_values.description = formatDescription(
					CSVToArray(description.toString()),
					this.props.League.languages
				);
				await this.props.updateLeagueGame(new_values);
			}
		});
	};
	moveToNext = () => {
		this.setState({ step: this.state.step + 1 });
	};
	moveBack = () => {
		this.setState({ step: this.state.step - 1 });
	};

	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		const { step, totalSteps } = this.state;
		const { fileList, descfileList, prizeFileList, ruleFileList } = this.state;
		const {
			isCreateLeagueLoading,
			isLeagueCreated,
			isCreateLeagueFailed,
			languages
		} = this.props.League;
		const props = {
			onRemove: file => {
				this.setState(state => {
					const index = state.fileList.indexOf(file);
					const newFileList = state.fileList.slice();
					newFileList.splice(index, 1);
					return {
						fileList: newFileList
					};
				});
			},
			beforeUpload: file => {
				const isImage = file.type === "image/png" || file.type === "image/jpeg";
				if (!isImage) {
					message.error("You can only upload Image file!");
					return false;
				}
				this.setState(state => ({
					fileList: [...state.fileList, file]
				}));
				return false;
			},
			fileList
		};
		const ruleProps = Object.keys(languages).reduce((a, lang) => {
			a[lang] = {
				onRemove: file => {
					this.setState(state => {
						const index = state.ruleFileList[lang].indexOf(file);
						const newFileList = state.ruleFileList[lang].slice();
						newFileList.splice(index, 1);
						return {
							ruleFileList: { ...state.ruleFileList, [lang]: newFileList }
						};
					});
				},
				beforeUpload: file => {
					const isJson = file.type === "application/json";
					if (!isJson) {
						message.error("You can only upload JSON file!");
						return false;
					}
					this.setState(state => ({
						ruleFileList: {
							...state.ruleFileList,
							[lang]: [...state.ruleFileList[lang], file]
						}
					}));
					return false;
				},
				fileList: ruleFileList[lang]
			};
			return a;
		}, {});
		const descProps = {
			onRemove: file => {
				this.setState(state => {
					const index = state.descfileList.indexOf(file);
					const newFileList = state.descfileList.slice();
					descfileList.splice(index, 1);
					return {
						fileList: descfileList
					};
				});
			},
			beforeUpload: file => {
				const isCSV = file.type === "text/csv";
				if (!isCSV) {
					message.error("You can only upload CSV file!");
					return false;
				}
				this.setState(state => ({
					descfileList: [...state.descfileList, file]
				}));
				return false;
			},
			filesList: descfileList
		};
		const prizeProps = {
			onRemove: file => {
				this.setState(state => {
					const index = state.prizeFileList.indexOf(file);
					const newFileList = state.prizeFileList.slice();
					prizeFileList.splice(index, 1);
					return {
						fileList: prizeFileList
					};
				});
			},
			beforeUpload: file => {
				const isCSV = file.type === "text/csv";
				if (!isCSV) {
					message.error("You can only upload CSV file!");
					return false;
				}
				this.setState(state => ({
					prizeFileList: [...state.prizeFileList, file]
				}));
				return false;
			},
			filesList: prizeFileList
		};
		const uploadButton = (
			<div>
				<Icon type={this.state.loading ? "loading" : "plus"} />
				<div className="ant-upload-text">Upload</div>
			</div>
		);
		return (
			<Form layout="inline" onSubmit={this.handleSubmit}>
				<div className="league_container">
					{/* <div className="league_header">
						<div className="league_save_progress" onClick={this.handleSubmit}>
							<p>Save Progress</p>
						</div>
					</div> */}
					<div className="league_content">
						<div className="game_container">
							<FormItem label={"name  "}>
								{getFieldDecorator("name", {
									rules: [{ required: true }]
								})(
									<Input
										addonBefore={<Icon type="rocket" />}
										className=" game_fix game_name"
										placeholder="Enter League Name "
									/>
								)}
							</FormItem>
							<FormItem label={"prize  "}>
								{getFieldDecorator("prize_money", {
									rules: [{ required: true }]
								})(
									<Input
										addonBefore={<Icon type="money-collect" />}
										className=" game_fix game_prize"
										type="number"
										min="1"
										placeholder="Enter Prize "
									/>
								)}
							</FormItem>
							<FormItem label={"BuyCoins  "}>
								{getFieldDecorator("buy_in", {
									rules: [{ required: true }]
								})(
									<Input
										addonBefore={<Icon type="dollar" />}
										className=" game_fix game_prize"
										type="number"
										min="1"
										placeholder="Buy In "
									/>
								)}
							</FormItem>
						</div>
						<div className="game_container">
							<FormItem label={"Starts At  "}>
								{getFieldDecorator("starts_at", {
									initialValue: new Date(),
									rules: [{ required: true }]
								})(<DateTimePicker />)}
							</FormItem>
							<FormItem label={"Ends At  "}>
								{getFieldDecorator("ends_at", {
									initialValue: new Date(),
									rules: [{ required: true }]
								})(<DateTimePicker />)}
							</FormItem>
							<FormItem label={"Results At  "}>
								{getFieldDecorator("result_announced_at", {
									initialValue: new Date(),
									rules: [{ required: true }]
								})(<DateTimePicker />)}
							</FormItem>
						</div>
						<div className="game_container">
							<FormItem label={"Image Url "} style={{ width: "100%" }}>
								{getFieldDecorator("image_url", {
									rules: [{ required: true }]
								})(
									<Upload {...props}>
										{" "}
										{fileList.length < 1 && (
											<Button>
												<Icon type="upload" /> Select File
											</Button>
										)}
									</Upload>
								)}
							</FormItem>
							<FormItem label={"Descriptions"} style={{ width: "100%" }}>
								{getFieldDecorator("description", {
									rules: [{ required: true }]
								})(
									<Upload {...descProps}>
										{" "}
										{descfileList.length < 1 && (
											<Button>
												<Icon type="upload" /> Select File
											</Button>
										)}
									</Upload>
								)}
							</FormItem>
							<FormItem label={"prize distribution"} style={{ width: "100%" }}>
								{getFieldDecorator("rank_prize", {
									rules: [{ required: true }]
								})(
									<Upload {...prizeProps}>
										{" "}
										{prizeFileList.length < 1 && (
											<Button>
												<Icon type="upload" /> Select File
											</Button>
										)}
									</Upload>
								)}
							</FormItem>
						</div>
						<div className="game_container">
							{Object.keys(languages).map(lang => (
								<FormItem label={`rules-${lang}`} style={{ width: "100%" }}>
									{getFieldDecorator(lang, {
										rules: [{ required: true }]
									})(
										<Upload {...ruleProps[lang]}>
											{" "}
											{ruleFileList[lang].length < 1 && (
												<Button>
													<Icon type="upload" /> Select File
												</Button>
											)}
										</Upload>
									)}
								</FormItem>
							))}
						</div>
					</div>
				</div>
				<div className="league_header">
					{isLeagueCreated ? (
						<div className="league_save_float league_save_success">
							<p style={{ margin: "0px", paddingLeft: "40%" }}>
								League Created Successfully
							</p>
						</div>
					) : isCreateLeagueFailed ? (
						<div className="league_save_float league_save_failed">
							<p style={{ margin: "0px", paddingLeft: "45%" }}>Try Again</p>
						</div>
					) : (
						<div className="league_save_progress" onClick={this.handleSubmit}>
							<p style={{ margin: "0px", paddingLeft: "10px" }}>
								Save Progress
							</p>
							{isCreateLeagueLoading && <Icon type="loading" />}
						</div>
					)}
				</div>
				{/* <div className="league_foooter">
					<div className="league_back_button" onClick={this.moveBack}>
						<Icon type="arrow-left" />
					</div>
					<div className="league_progress">
						<div
							className="league_progess_bar"
							style={{
								background: `linear-gradient(to right,#3d6dfe ${(step /
									totalSteps) *
									100}%,#e4e9f1 ${(step / totalSteps) * 100}%)`
							}}
						/>
					</div>
					{step < totalSteps && (
						<div className="league_next_button" onClick={this.moveToNext}>
							NEXT
							<Icon style={{ marginleft: "10px" }} type="arrow-right" />
						</div>
					)}
				</div> */}
				{/* <div className="extra_space" /> */}
			</Form>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		s3Upload: file => dispatch(s3Upload(file)),
		setCreateLeagueLoading: bool => dispatch(setCreateLeagueLoading(bool)),
		updateLeagueGame: values => dispatch(updateLeagueGame(values))
	};
};
const mapStateToProps = state => ({
	League: state.League
});
const WrappedLeagueForm = Form.create()(CreateLeague);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedLeagueForm);
