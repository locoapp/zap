import React, { Component } from "react";
import { connect } from "react-redux";
import { Skeleton, Carousel } from "antd";
import { fetchLeagueQuestions, setQuestionLoading } from "../../actions/league";
import "./index.css";
class SuccessModal extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div className="league_container success_league_content">
				{" "}
				<div className="game_container" />
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {};
};
const mapStateToProps = state => ({});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SuccessModal);
