import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Icon, Form, Button, Upload } from "antd";
import DateTimePicker from "react-widgets/lib/DateTimePicker";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
import "react-widgets/dist/css/react-widgets.css";

const FormItem = Form.Item;
Moment.locale("en");
momentLocalizer();
function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}
class StepOne extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		document.getElementsByClassName("ant-modal-body")[0].style.padding = "0px";
		document.getElementsByClassName("ant-modal-body")[0].style.backgroundColor =
			"#f6f7fa";
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		let elements = document.getElementsByClassName("ant-form-item");
		for (let ele of elements) {
			ele.style.display = "flex";
			ele.style.flexDirection = "column";
		}
		let items = document.getElementsByClassName("ant-form-item-label");
		for (let it of items) it.style.textAlign = "left";
	}
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				console.log(values);
			}
		});
	};

	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		return (
			<Form layout="inline" onSubmit={this.handleSubmit}>
				<div className="league_content">
					<div className="game_container">
						<FormItem label={"name  "}>
							{getFieldDecorator("name", {
								rules: [{ required: true }]
							})(
								<Input
									addonBefore={<Icon type="rocket" />}
									className=" game_fix game_name"
									placeholder="Enter League Name "
								/>
							)}
						</FormItem>
						<FormItem label={"prize  "}>
							{getFieldDecorator("prize_money", {
								rules: [{ required: true }]
							})(
								<Input
									addonBefore={<Icon type="money-collect" />}
									className=" game_fix game_prize"
									type="number"
									min="1"
									placeholder="Enter Prize "
								/>
							)}
						</FormItem>
						<FormItem label={"BuyCoins  "}>
							{getFieldDecorator("buy_in", {
								rules: [{ required: true }]
							})(
								<Input
									addonBefore={<Icon type="dollar" />}
									className=" game_fix game_prize"
									type="number"
									min="1"
									placeholder="Buy In "
								/>
							)}
						</FormItem>
					</div>
					<div className="game_container">
						<FormItem label={"Starts At  "}>
							{getFieldDecorator("starts_at", {
								initialValue: new Date(),
								rules: [{ required: true }]
							})(<DateTimePicker />)}
						</FormItem>
						<FormItem label={"Ends At  "}>
							{getFieldDecorator("ends_at", {
								initialValue: new Date(),
								rules: [{ required: true }]
							})(<DateTimePicker />)}
						</FormItem>
						<FormItem label={"Results At  "}>
							{getFieldDecorator("result_announced_at", {
								initialValue: new Date(),
								rules: [{ required: true }]
							})(<DateTimePicker />)}
						</FormItem>
					</div>
					<div className="game_container">
						<FormItem label={"Image Url"} style={{ width: "100%" }}>
							{getFieldDecorator("image_url", {
								rules: [{ required: true }]
							})(
								<Upload listType="picture-card">
									<div>
										<Icon type="plus" />
										<div className="ant-upload-text">Upload</div>
									</div>
								</Upload>
							)}
						</FormItem>
					</div>
				</div>
			</Form>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {};
};
const mapStateToProps = state => ({});
const WrappedOneForm = Form.create()(StepOne);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedOneForm);
