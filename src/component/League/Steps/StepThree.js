import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Icon, Form, Button, Upload } from "antd";
import DateTimePicker from "react-widgets/lib/DateTimePicker";
import Moment from "moment";
import "./index.css";
import momentLocalizer from "react-widgets-moment";
import "react-widgets/dist/css/react-widgets.css";

const FormItem = Form.Item;
Moment.locale("en");
momentLocalizer();
function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}
class StepThree extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		document.getElementsByClassName("ant-modal-body")[0].style.padding = "0px";
		document.getElementsByClassName("ant-modal-body")[0].style.backgroundColor =
			"#f6f7fa";
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		let elements = document.getElementsByClassName("ant-form-item");
		for (let ele of elements) {
			ele.style.display = "flex";
			ele.style.flexDirection = "column";
		}
		let items = document.getElementsByClassName("ant-form-item-label");
		for (let it of items) it.style.display = "none";
	}
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				console.log(values);
			}
		});
	};
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				console.log(values);
			}
		});
	};

	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		const { languages } = this.props.League;
		return (
			<Form layout="inline" onSubmit={this.handleSubmit}>
				<div
					className="league_content"
					style={{ padding: "40px", display: "flex", flexDirection: "row" }}
				>
					<FormItem label={"rule"}>
						{getFieldDecorator("rule", {
							rules: [{ required: true }]
						})(
							<Upload listType="picture-card">
							 {fileList.length >= 3 ? null 	<div>
									<Icon type="plus" />
									<div className="ant-upload-text">{"up"}</div>
								</div>
							 }
							</Upload>
						)}
					</FormItem>

					<div className="league_save_progress" onClick={this.handleSubmit}>
						<p>Save Progress</p>
					</div>
				</div>
			</Form>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {};
};
const mapStateToProps = state => ({ League: state.League });
const WrappedOneForm = Form.create()(StepThree);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedOneForm);
