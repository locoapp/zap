import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Icon, Form, Button } from "antd";
import DateTimePicker from "react-widgets/lib/DateTimePicker";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
import "react-widgets/dist/css/react-widgets.css";

const FormItem = Form.Item;
Moment.locale("en");
momentLocalizer();
function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}
class StepTwo extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	componentDidMount() {
		document.getElementsByClassName("ant-modal-body")[0].style.padding = "0px";
		document.getElementsByClassName("ant-modal-body")[0].style.backgroundColor =
			"#f6f7fa";
		document.getElementsByClassName("ant-modal-footer")[0].style.display =
			"none";
		let elements = document.getElementsByClassName("ant-form-item");
		for (let ele of elements) {
			ele.style.display = "flex";
			ele.style.flexDirection = "column";
		}
		let items = document.getElementsByClassName("ant-form-item-label");
		for (let it of items) it.style.display = "none";
	}
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				console.log(values);
			}
		});
	};

	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		const { languages } = this.props.League;
		return (
			<Form layout="inline" onSubmit={this.handleSubmit}>
				<div className="league_content" style={{ padding: "40px" }}>
					{Object.keys(languages).map(lang => (
						<FormItem label={lang}>
							{getFieldDecorator(languages[lang], {
								rules: [{ required: true }]
							})(
								<Input
									style={{ width: "80%", margin: "20px" }}
									addonBefore={lang}
									className=" game_fix game_name"
									placeholder={`Enter ${lang} description `}
								/>
							)}
						</FormItem>
					))}
				</div>
			</Form>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {};
};
const mapStateToProps = state => ({ League: state.League });
const WrappedOneForm = Form.create()(StepTwo);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WrappedOneForm);
