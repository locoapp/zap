import React from 'react'
import {connect} from 'react-redux'
import {createCampaign} from '../../actions'
import { Table , Button,Collapse } from 'antd';
import moment from 'moment'
import './index.css'

const Panel = Collapse.Panel;


 const CampaignList = ({campaigns,Defaults,createCampaign}) => {

    const columns = [{
        title: 'Language',
        dataIndex: 'language'
      }, {
        title: 'Title',
        dataIndex: 'title',
      }, {
        title: 'Message',
        dataIndex: 'message',
      },
      {
        title: 'Time',
        dataIndex: 'time',
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => {
            let {is_campaign_created,is_loading} = campaigns[text['key']]
          return(

              is_campaign_created
              ?
              <Button value="large" style={{background:'#87d068',color:'white'}} >
              created
              </Button>
              :

            <Button type="primary"  ghost onClick={() => createCampaign(text.key)} loading={is_loading}>
            send
            </Button>
                  
          )
                  
        },
      }
    ];
     const data = campaigns.map((campaign,index) => ({
         "key":index,
         "language":campaign.name.split("-")[0],
         "title":campaign.content.title,
         "message":campaign.content.body,
         "time":moment(campaign.when,`${Defaults.defaultDateFormat} ${Defaults.defaultTimeFormat}`).format(`${Defaults.defaultNameDateFormat} ${Defaults.defaultTimeFormat}`)
        }))
    return(
        <div>
       <Table pagination={false} columns={columns} dataSource={data} />
       <Collapse >
        <Panel header="Developer view" key="1" >
        {campaigns.map(campaign => 
          <pre className="language-bash">
          {JSON.stringify(campaign, null, 2)}
        </pre>
        )}
          
        </Panel>
       
        </Collapse>
        </div>



    )
}

const mapDispatchToProps = dispatch => {
    return {
        createCampaign : (index) => dispatch(createCampaign(index))
    }
}

const mapStateToProps = state => ({
    campaigns:state.campaigns,
    Defaults:state.Defaults
})



export default connect(mapStateToProps,mapDispatchToProps)(CampaignList)