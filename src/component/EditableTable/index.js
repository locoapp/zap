import {
	Table,
	Input,
	Button,
	Form,
	Popconfirm,
	Col,
	TimePicker,
	Select
} from "antd";
import moment from "moment";
import React from "react";
import { connect } from "react-redux";
import {
	tableAdd,
	tableDelete,
	tableSave,
	alterConfirmCampaignModal,
	createAllCampaigns,
	changeTime,
	changeType
} from "../../actions";
import ConfirmCampaignsModal from "../Modals/ConfirmCampaignsModal";
import CampaignList from "./CampaignList";
const FormItem = Form.Item;
const EditableContext = React.createContext();
const Option = Select.Option;

const EditableRow = ({ form, index, ...props }) => (
	<EditableContext.Provider value={form}>
		<tr {...props} />
	</EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
	state = {
		editing: false
	};

	componentDidMount() {
		if (this.props.editable) {
			document.addEventListener("click", this.handleClickOutside, true);
		}
	}

	componentWillUnmount() {
		if (this.props.editable) {
			document.removeEventListener("click", this.handleClickOutside, true);
		}
	}

	toggleEdit = () => {
		const editing = !this.state.editing;
		const { record, handleSave, dataIndex } = this.props;
		this.setState({ editing }, () => {
			if (editing) {
				if (
					this.input.props.value.includes("Enter the title") ||
					this.input.props.value.includes("Enter the message")
				) {
					handleSave({ ...record, [dataIndex]: "" });
				}
				this.input.focus();
			}
		});
	};

	handleClickOutside = e => {
		const { editing } = this.state;
		if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
			this.save();
		}
	};

	save = () => {
		const { record, handleSave } = this.props;
		this.form.validateFields((error, values) => {
			if (error) {
				return;
			}
			this.toggleEdit();
			handleSave({ ...record, ...values });
		});
	};

	render() {
		const { editing } = this.state;
		const {
			editable,
			dataIndex,
			title,
			record,
			index,
			handleSave,
			...restProps
		} = this.props;
		return (
			<td ref={node => (this.cell = node)} {...restProps}>
				{editable ? (
					<EditableContext.Consumer>
						{form => {
							this.form = form;
							return editing ? (
								<FormItem style={{ margin: 0 }}>
									{form.getFieldDecorator(dataIndex, {
										rules: [
											{
												required: true,
												message: `${title} is required.`
											}
										],
										initialValue: record[dataIndex]
									})(
										<Input
											placeholder=" Enter the Text"
											ref={node => (this.input = node)}
											onPressEnter={this.save}
										/>
									)}
								</FormItem>
							) : (
								<div
									className="editable-cell-value-wrap"
									// style={{ paddingRight: 24 }}
									onClick={this.toggleEdit}
								>
									{restProps.children}
								</div>
							);
						}}
					</EditableContext.Consumer>
				) : (
					restProps.children
				)}
			</td>
		);
	}
}

class EditableTable extends React.Component {
	constructor(props) {
		super(props);
		this.columns = [
			{
				title: "Language",
				dataIndex: "language",
				render: (text, record) => {
					return (
						<Col span={4}>
							<Select
								style={{ width: 150 }}
								defaultValue="All"
								onChange={value =>
									this.handleSave({ ...record, language: value })
								}
							>
								{this.props.Defaults.languages.map((language, index) => (
									<Option key={language} value={language}>
										{language}
									</Option>
								))}
							</Select>
						</Col>
					);
				}
			},
			...this.props.Table.columns,
			{
				title: "Type",
				dataIndex: "type",
				render: (text, record) => {
					return (
						<Col span={4}>
							<Select
								style={{ width: 150, textAlign: "center" }}
								defaultValue="Select QUIZ"
								onChange={value => this.handleSave({ ...record, type: value })}
							>
								{this.props.Defaults.quiz_types.map((type, index) => (
									<Option key={type} value={type}>
										{type}
									</Option>
								))}
							</Select>
						</Col>
					);
				}
			},
			{
				title: "Time",
				dataIndex: "time",
				render: (text, record) => {
					return (
						<Col span={6}>
							<TimePicker
								onChange={value => this.handleSave({ ...record, time: value })}
								defaultValue={moment(
									record.time,
									this.props.Defaults.defaultTimeFormat
								)}
								format={this.props.Defaults.defaultTimeFormat}
							/>
						</Col>
					);
				}
			},
			{
				title: "ACTION",
				dataIndex: "Action",
				render: (text, record) => {
					return (
						this.props.isLanguageFieldActive && (
							<Popconfirm
								title="Sure to delete?"
								onConfirm={() => this.props.tableDelete(record)}
							>
								<Button type="danger" ghost icon="delete" />
							</Popconfirm>
						)
					);
				}
			}
		];
	}

	handleOk = () => {
		this.props.createAllCampaigns();
	};
	handleSave = row => {
		const type = this.props.isLanguageFieldActive
			? "dataSource"
			: "allDataSource";
		this.props.tableSave(row, type);
	};
	handleCancel = () => {
		this.props.alterConfirmCampaignModal(false);
	};

	render() {
		const dataSource = !this.props.isContestCampaignModalActive
			? this.props.Table.dataSource
			: this.props.Table.dataSource.map(data => {
					data["uid"] = this.props.contestForCampaign.key;
					return data;
			  });
		const { state } = this.props;
		const components = {
			body: {
				row: EditableFormRow,
				cell: EditableCell
			}
		};
		const COLUMNS = this.columns.map(col => {
			if (!col.editable) {
				return col;
			}
			return {
				...col,
				onCell: record => ({
					record,
					editable: col.editable,
					dataIndex: col.dataIndex,
					title: col.title,
					handleSave: this.handleSave
				})
			};
		});
		return (
			<div>
				<Button
					size="large"
					style={{ margin: "20px 0px" }}
					type="primary"
					onClick={this.props.tableAdd}
				>
					Add Campaign
				</Button>

				<ConfirmCampaignsModal
					title="Confirm and Send Clevertap Campaign"
					createAllClicked={this.props.createAllClicked}
					visible={this.props.isConfirmCampaignsActive}
					handleOk={this.handleOk}
					handleCancel={this.handleCancel}
				>
					<CampaignList />
				</ConfirmCampaignsModal>
				<Table
					components={components}
					rowClassName={() => "editable-row"}
					bordered
					dataSource={dataSource}
					columns={COLUMNS}
					pagination={false}
				/>
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		tableAdd: () => dispatch(tableAdd()),
		changeType: value => dispatch(changeType(value)),
		changeTime: time => dispatch(changeTime(time)),
		createAllCampaigns: () => dispatch(createAllCampaigns()),
		tableDelete: record => dispatch(tableDelete(record)),
		tableSave: (row, type) => {
			dispatch(tableSave(row, type));
		},
		alterConfirmCampaignModal: payload =>
			dispatch(alterConfirmCampaignModal(payload))
	};
};
const mapStateToProps = state => {
	return {
		Table: state.Table,
		Defaults: state.Defaults,
		isContestCampaignModalActive: state.Contests.isContestCampaignModalActive,
		contestForCampaign: state.Contests.contestForCampaign,
		createAllClicked: state.Settings.createAllClicked,
		isLanguageFieldActive: state.Settings.isLanguageFieldActive,
		isConfirmCampaignsActive: state.Settings.isConfirmCampaignsActive
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditableTable);
