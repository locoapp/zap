import React from'react'
import { connect } from 'react-redux'
import {changeType} from '../../actions'
import moment from 'moment';
import {Row, Col,Input, Select,DatePicker} from 'antd';
const InputGroup = Input.Group;
const Option = Select.Option;


export const TitleEvent = ({Defaults,changeType}) => {
       const {languages,date,quiz_types,time,selected_language,defaultNameDateFormat} = Defaults
       
       const handleChange = (value) => {
         changeType(value)
       }
        return ( 
           <div style={{margin:'20px 0px'}}>
            <Row> 
                <h4>Title</h4>
                {/* <Col span={3}>
                        <Select style={{ width: 120 }}  defaultValue="All" onChange={(value) => handleChange(value,"language")}>
                            {
                                languages.map(language =>   <Option key={language} value={language}>{language}</Option>)
                            }
                        </Select>
                </Col>   */}
                {/* <Col span={3}> 
                    <DatePicker style={{ width: 120 }} defaultValue={moment(date,defaultNameDateFormat)} format={defaultNameDateFormat} onChange={(date, dateString) => handleChange(date,"date")} />        
                </Col>    */}
                <Col span={3}> 
                    <Select style={{ width: 120 }}  defaultValue="Day" onChange={(value) => handleChange(value)}>
                        {
                            quiz_types.map((type,index) =>   <Option key={type} value={type}>{type}</Option>)
                        }
                    </Select>
                </Col>    
            
            </Row>
               
           </div>
         );
    }
 
const mapDispatchToProps = dispatch => {
    return {
        changeType: (value) => dispatch(changeType(value))
    }
}
const mapStateToProps = state => {
    return {
        Defaults:state.Defaults,
    }
}




export default connect(mapStateToProps,mapDispatchToProps)(TitleEvent);