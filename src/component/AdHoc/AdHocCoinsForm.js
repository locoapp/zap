import React,{Component} from 'react'
import {connect} from 'react-redux'
import { grantCoins } from '../../actions/adhoc'

import { Form, Icon, Input, Button } from 'antd';
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}


class CoinForm  extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            values['amount'] = parseInt(values['amount'])
           this.props.grantCoins(values)
          }
        });
      }
    render() { 
        const {
            getFieldDecorator, getFieldsError, getFieldError, isFieldTouched,
          } = this.props.form;
      
          // Only show error after a field is touched.
          const userNameError = isFieldTouched('username') && getFieldError('username');
          const amountError = isFieldTouched('amount') && getFieldError('amount');
          const reasonError = isFieldTouched('reason') && getFieldError('reason');

        return ( 
                <div style={{display:'flex',flexDirection:'column',margin:'4%'}}>
                <h3>Grant coins</h3>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                        <FormItem
                        validateStatus={userNameError ? 'error' : ''}
                        help={userNameError || ''}
                        >
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'Please input  username!' }],
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                        )}
                        </FormItem>
                        <FormItem
                        validateStatus={amountError ? 'error' : ''}
                        help={amountError || ''}
                        >
                        {getFieldDecorator('amount', {
                            rules: [{ required: true, message: 'Please input the amount of coins' }],
                        })(
                            <Input prefix={<Icon type="dollar" style={{ color: 'rgba(0,0,0,.25)' }} />} type="number" placeholder="Amount" />
                        )}
                        </FormItem>
                        <FormItem
                        validateStatus={reasonError ? 'error' : ''}
                        help={reasonError || ''}
                        >
                        {getFieldDecorator('reason', {
                            rules: [{ required: true, message: 'Please tell reason' }],
                        })(
                            <Input prefix={<Icon type="question-circle" style={{ color: 'rgba(0,0,0,.25)' }} />}  placeholder="Reason" />
                        )}
                        </FormItem>
                        <FormItem>
                        <Button
                            type="primary"
                            htmlType="submit"
                            loading={this.props.isGrantCoinLoading}
                            disabled={hasErrors(getFieldsError())}
                        >
                            Grant
                        </Button>
                        </FormItem>
                    </Form>
                    </div>
         );
    }
}
const mapDispatchToProps = dispatch => {
  return {
      grantCoins: (values) => dispatch(grantCoins(values))
  }
}
const mapStateToProps = state => ({
    isGrantCoinLoading:state.Adhoc.isGrantCoinLoading
})

const AdHocCoinsForm = Form.create()(CoinForm);

export default connect(mapStateToProps,mapDispatchToProps)(AdHocCoinsForm);

