import React,{Component} from 'react'
import {connect} from 'react-redux'
import AdHocCoinsForm from './AdHocCoinsForm';
class AdHoc  extends Component {

 

    showContestModal= () => {
        this.props.alterContestModal(true)
    }
    handleCancel = () => {
        this.props.alterContestModal(false)
    }

  
    render() { 
        return ( 
            <div>
                    <AdHocCoinsForm />
              
            </div>
         );
    }
}
const mapDispatchToProps = dispatch => {
  return {
  }
}
const mapStateToProps = state => ({
  
})
export default connect(mapStateToProps,mapDispatchToProps)(AdHoc);