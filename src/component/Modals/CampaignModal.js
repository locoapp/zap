import React from'react'

import {Modal,Button} from 'antd';


export const CampaignModal = ({children,title,visible,handleOk,handleCancel,isCreateCampaignButtonActive}) => {
        return ( 
            <Modal
                title={title}
                visible={visible}
                onCancel={handleCancel}
                width={"80%"}
                maskClosable={false}
                footer={[
                    <Button disabled={!isCreateCampaignButtonActive}  style={{height:'40px',fontSize:'16px',background:'#52c41a',outline:'none',border:'none'}} key="submit" type="primary"  onClick={handleOk}>
                      Create Campaign
                    </Button>,
                  ]}
            >
             {children}
            </Modal>
         );
    }
 
export default CampaignModal;