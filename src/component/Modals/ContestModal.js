import React from'react'

import {Modal,Button} from 'antd';


export const ContestModal = ({children,title,visible,handleOk,handleCancel,isContestModalActive}) => {
        return ( 
            <Modal
                title={title}
                visible={visible}
                onCancel={handleCancel}
                width={"80%"}
                footer={null}
                maskClosable={false}
            >
             {children}
            </Modal>
         );
    }
 
export default ContestModal;