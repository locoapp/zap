import React from "react";
import { Modal, Button } from "antd";

export const ContestCampaignModal = ({
	children,
	title,
	createAllClicked,
	visible,
	handleOk,
	handleCancel
}) => {
	return (
		<Modal
			title={title}
			visible={visible}
			onCancel={handleCancel}
			width={"80%"}
			maskClosable={false}
		>
			{children}
		</Modal>
	);
};

export default ContestCampaignModal;
