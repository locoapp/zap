import React from'react'
import {Modal,Button} from 'antd';


export const ConfirmCampaignsModal = ({children,title,createAllClicked,visible,handleOk,handleCancel}) => {
        return ( 
            <Modal
                title={title}
                visible={visible}
                onCancel={handleCancel}
                width={"80%"}
                maskClosable={false}
                footer={[
                    <Button  style={{height:'40px',fontSize:'16px',outline:'none'}} key="submit" type="primary" ghost  onClick={handleCancel}>
                      Go Back
                    </Button>,
                    <Button  style={{height:'40px',fontSize:'16px',outline:'none',border:'none'}} key="submit" type="primary" disabled={createAllClicked}  onClick={handleOk}>
                      Create All
                    </Button>,
                  ]}
            >
             {children}
            </Modal>
         );
    }
 
export default ConfirmCampaignsModal;