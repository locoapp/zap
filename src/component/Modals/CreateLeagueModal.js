import React from "react";

import { Modal, Button } from "antd";

const CreateLeagueModal = ({
	children,
	title,
	visible,
	handleOk,
	handleCancel
}) => {
	return (
		<Modal
			title={title}
			visible={visible}
			onCancel={handleCancel}
			width={"80%"}
			maskClosable={false}
			footer={[]}
		>
			{children}
		</Modal>
	);
};

export default CreateLeagueModal;
