import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import Campaigns from "./index";
import WhenEvent from "../WhenEvent";
import TitleEvent from "../TitleEvent";
import EditableTable from "../EditableTable";
import { addCampaign } from "../../actions/addCampaign";

import { Table, Skeleton, Row, Button } from "antd";
import { fetchCampaigns, setCampaignsLoading } from "../../actions";
class ContestCampaign extends Component {
	handleOk = e => {
		this.props.addCampaign();
	};

	render() {
		const { isCampaignsLoading, is_prize_distributed } = this.props;
		return (
			<div>
				{!is_prize_distributed && (
					<div style={{ margin: "0.1%" }}>
						<WhenEvent start_time={this.props.start_time} />
						<EditableTable />
						<Row style={{ margin: "40px", float: "right" }}>
							<Button
								disabled={!this.props.isCreateCampaignButtonActive}
								style={{
									height: "40px",
									fontSize: "16px",
									background: "#52c41a",
									outline: "none",
									border: "none"
								}}
								key="submit"
								type="primary"
								onClick={this.handleOk}
							>
								Create Campaign
							</Button>
						</Row>
					</div>
				)}
				<Row
					style={{
						marginTop: `${!is_prize_distributed ? "140px" : "10px"}`,
						borderTop: `${!is_prize_distributed && "1px solid #1a1a1a"}`
					}}
				>
					<h2 style={{ margin: "20px" }}>Scheduled Campaigns</h2>
				</Row>
				<Campaigns uid={this.props.uid} />
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		addCampaign: () => dispatch(addCampaign())
	};
};
const mapStateToProps = state => ({
	isCreateCampaignButtonActive:
		(state.Settings.isLanguageFieldActive &&
			state.Table.dataSource.length > 0) ||
		(!state.Settings.isLanguageFieldActive &&
			state.Table.allDataSource.length > 0),
	campaignColumns: state.Table.campaignColumns,
	campaigns: state.Targets.campaigns,
	isCampaignsLoading: state.Table.isCampaignsLoading
});
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ContestCampaign);
