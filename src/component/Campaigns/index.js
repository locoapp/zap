import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Table, Skeleton } from "antd";
import { fetchCampaigns, setCampaignsLoading } from "../../actions";
class Campaigns extends Component {
	async componentDidMount() {
		if (this.props.uid == undefined || this.props.uid == null) {
			await this.props.fetchCampaigns();
		}
		this.props.setCampaignsLoading(false);
	}
	render() {
		const { isCampaignsLoading, dataSource, columns } = this.props;
		return (
			<div style={{ margin: "0.1%" }}>
				{isCampaignsLoading ? (
					<Skeleton active />
				) : (
					<Table dataSource={dataSource} columns={columns} />
				)}
			</div>
		);
	}
}
const mapDispatchToProps = dispatch => {
	return {
		fetchCampaigns: () => dispatch(fetchCampaigns()),
		setCampaignsLoading: bool => dispatch(setCampaignsLoading(bool))
	};
};
const mapStateToProps = state => ({
	columns: state.Table.campaignColumns,
	dataSource: state.Targets.dataSource,
	isCampaignsLoading: state.Table.isCampaignsLoading
});
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Campaigns);
