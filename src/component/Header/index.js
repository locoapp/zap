import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Auth } from 'aws-amplify';
import {addCampaign,confirmCampaigns} from '../../actions/addCampaign'
import {alterCampaignModal,alterConfirmCampaignModal} from '../../actions'
import './index.css'
import { Avatar, Button ,Icon} from 'antd';

import {CampaignModal} from '../Modals/CampaignModal'
import EditableTable from '../EditableTable'
import WhenEvent from '../WhenEvent'
import TitleEvent from '../TitleEvent'
class Header  extends Component {

  constructor(props) {
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    Auth.signOut();
    localStorage.removeItem("tokens")
    localStorage.removeItem("auth_key")
    localStorage.removeItem("phone")
    window.location.reload()
  }
  showModal = () => {
   this.props.alterCampaignModal(true)
  }

  handleOk = (e) => {
    this.props.addCampaign()
  }
  

  handleCancel = (e) => {
   this.props.alterCampaignModal(false)
  }
    render() { 
        return ( 
            <div className="header_container">
                <div className="header_logo">
                
                <img alt="logo"  src="./zap-logo.svg" />
                </div>
                <div className="create_campaign_button">
                    <Button  size="large" type="primary" onClick={this.showModal}>CREATE CAMPAIGN</Button>
                    <Avatar onClick={this.signOut} style={{ backgroundColor: '#f56a00', verticalAlign: 'middle',float:'right',cursor:'pointer' }} size={40}>
                    <Icon type="logout" />
                  </Avatar>
                    <CampaignModal title="Create Clevertap Campaign"
                            visible={this.props.isCreateCampaignActive}
                            handleOk={this.handleOk}
                            isCreateCampaignButtonActive={this.props.isCreateCampaignButtonActive}
                            handleCancel={this.handleCancel} >
                      {/* <TitleEvent/> */}
                      <WhenEvent />
                      <EditableTable />
                    </CampaignModal>
                </div>
            </div>
         );
    }
}
const mapDispatchToProps = dispatch => {
  return {
    addCampaign : () => dispatch(addCampaign()),
    alterConfirmCampaignModal : (payload) => dispatch(alterConfirmCampaignModal(payload)),
    alterCampaignModal : (verdict) => dispatch(alterCampaignModal(verdict))
  }
}
const mapStateToProps = state => ({
 
  isCreateCampaignButtonActive:(state.Settings.isLanguageFieldActive && state.Table.dataSource.length>0) || (!state.Settings.isLanguageFieldActive && state.Table.allDataSource.length>0)  ,
  isCreateCampaignActive:state.Settings.isCreateCampaignActive
})
export default connect(mapStateToProps,mapDispatchToProps)(Header);