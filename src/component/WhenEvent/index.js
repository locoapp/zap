import React from "react";
import { connect } from "react-redux";
import { changeDate, toggleLanguageField, changeTime } from "../../actions";

import { Row, Col, DatePicker, TimePicker, Tag, Switch, Popover } from "antd";
import moment from "moment";

const format = "HH:mm";
let dateValidator = false;
export const WhenEvent = ({
	Defaults,
	isLanguageFieldActive,
	changeDate,
	toggleLanguageField,
	changeTime,
	start_time
}) => {
	const { devices, time, defaultDateFormat, defaultTimeFormat } = Defaults;
	let { date } = Defaults;
	function onChange(date) {
		changeDate(date);
	}
	let isDisabled = start_time != undefined && start_time != null;
	if (isDisabled && !dateValidator) {
		dateValidator = true;
		changeDate(moment(start_time));
	}

	return (
		<div style={{ margin: "20px 0px" }}>
			<Row>
				<Col span={6}>
					{" "}
					<h4>Date</h4>
					<DatePicker
						defaultValue={moment(date, defaultDateFormat)}
						format={defaultDateFormat}
						disabled={isDisabled}
						onChange={onChange}
					/>
				</Col>
				{/* <Col span={6}><h4>Time</h4><TimePicker  onChange={onChange} defaultValue={moment(time,defaultTimeFormat)} format={defaultTimeFormat} onChange={changeTime} /></Col> */}
				<Col span={6}>
					{" "}
					<h4>Devices</h4>
					{devices.map(device => (
						<Tag key={device} color="#87d068">
							{device}
						</Tag>
					))}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = state => {
	return {
		Defaults: state.Defaults,
		isLanguageFieldActive: state.Settings.isLanguageFieldActive
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeDate: date => dispatch(changeDate(date)),
		changeTime: time => dispatch(changeTime(time)),
		toggleLanguageField: checked => {
			dispatch(toggleLanguageField(checked));
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WhenEvent);
