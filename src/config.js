export default {
	apiGateway: {
		REGION: "ap-south-1",
		devURL:
			"https://nb6dcibbl8.execute-api.ap-south-1.amazonaws.com/dev/v1/clevertap/",
		prodURL: "https://zap-sl.getloconow.com/v1/clevertap/"
	},
	contest: {
		REGION: "ap-south-1",
		devURL:
			"https://nb6dcibbl8.execute-api.ap-south-1.amazonaws.com/dev/v1/contests/",
		prodURL: "https://zap-sl.getloconow.com/v1/contests/"
	},
	league: {
		REGION: "ap-south-1",
		devURL: "http://dev-league.getloconow.com/v1/games/admin/",
		prodURL: "https://league.getloconow.com/v1/games/admin/"
	},
	s3: {
		REGION: "ap-south-1",
		BUCKET: "showtime-production"
	}
};
