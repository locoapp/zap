import React from "react";
import ReactDOM from "react-dom";
import Amplify, { Auth } from "aws-amplify";
import config from "./config";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

Amplify.configure({
	Auth: {
		region: "ap-south-1",
		userPoolId: "ap-south-1_lsNJqvLOX",
		userPoolWebClientId: "5adhk6v3i8gbmaqat73oebdpvo",
		mandatorySignIn: false,
		identityPoolId: "ap-south-1:6cc6b02a-017f-4121-8a62-2b98124c7230"
	},
	Storage: {
		region: config.s3.REGION,
		bucket: config.s3.BUCKET,
		identityPoolId: "ap-south-1:6cc6b02a-017f-4121-8a62-2b98124c7230"
	},
	API: {
		endpoints: [
			{
				name: "clevertap",
				endpoint:
					process.env.NODE_ENV === "development"
						? config.apiGateway.devURL
						: config.apiGateway.prodURL,
				region: config.apiGateway.REGION,
				custom_header: async () => {
					return {
						Authorization: (await Auth.currentSession()).idToken.jwtToken
					};
				}
			},
			{
				name: "contest",
				endpoint:
					process.env.NODE_ENV === "development"
						? config.contest.devURL
						: config.contest.prodURL,
				region: config.apiGateway.REGION,
				custom_header: async () => {
					return {
						Authorization: (await Auth.currentSession()).idToken.jwtToken
					};
				}
			},
			{
				name: "leagues",
				endpoint:
					process.env.NODE_ENV === "development"
						? config.league.devURL
						: config.league.prodURL,
				region: config.apiGateway.REGION,
				custom_header: async () => {
					return {
						Authorization: (await Auth.currentSession()).idToken.jwtToken
					};
				}
			}
		]
	}
});

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
