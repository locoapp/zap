import React, { Component, lazy, Suspense } from "react";
import "./App.css";
import "./component/index.css";
import { withAuthenticator } from "aws-amplify-react";
import { Auth } from "aws-amplify";
import { default as MainHeader } from "./component/Header";
import store from "./store";
import { Provider } from "react-redux";

import Contests from "./component/Contests";
import Campaigns from "./component/Campaigns";
import League from "./component/League";
import AdHoc from "./component/AdHoc";
import { Tabs, Skeleton } from "antd";

// const Contests = lazy(() => import("./component/Contests"));
// const Campaigns = lazy(() => import("./component/Campaigns"));
// const League = lazy(() => import("./component/League"));

const TabPane = Tabs.TabPane;

store.subscribe(() => {
	// console.log(store.getState())
});

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSupport: false
		};
	}

	async componentDidMount() {
		try {
			let isSupport =
				(await Auth.currentSession()).idToken.payload["cognito:groups"].indexOf(
					"support"
				) > -1;
			this.setState({ isSupport: isSupport });
		} catch (e) {
			this.setState({ isSupport: false });
		}
	}
	render() {
		function callback(key) {
			console.log(key);
		}
		const { isSupport } = this.state;
		return (
			<Provider store={store}>
				<div className="App_container">
					<div className="App">
						<MainHeader />
						<Tabs defaultActiveKey="1" onChange={callback}>
							<TabPane tab="Contests" key="1">
								<Contests />{" "}
							</TabPane>
							<TabPane tab="Leagues" key="2">
								{" "}
								<League />
							</TabPane>
							<TabPane tab="Campaigns" key="3">
								<Campaigns />
							</TabPane>
							{isSupport && (
								<TabPane tab="Coins" key="4">
									<AdHoc />
								</TabPane>
							)}
						</Tabs>
					</div>
				</div>
			</Provider>
		);
	}
}

const MyTheme = {
	googleSignInButton: {
		backgroundImage: "linear-gradient(45deg,#843cf7,#38b8f2)"
	},
	formSection: { backgroundColor: "grey" },
	signInButtonIcon: {
		height: "44px",
		width: "44px",
		margin: "8px",
		borderRadius: "50%"
	},
	signInButtonContent: { fontSize: "18px" },
	button: { backgroundImage: "linear-gradient(45deg,#843cf7,#38b8f2)" },
	sectionBody: {
		fontFamily: "Rubik,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif"
	},
	hint: { fontFamily: "Rubik,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif" }
};

const AppWithAuth = withAuthenticator(App, false, [], null, MyTheme);

const federated = {
	google_client_id: ""
};
const ZAP = () => <AppWithAuth />;
export default ZAP;
